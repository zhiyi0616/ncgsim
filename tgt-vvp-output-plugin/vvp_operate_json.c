
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>


# include  "vvp_priv.h"
#include "cJSON.h"

char outbuf[BUFSIZ];
char fputc_outbuf[SIZE_FPUTC];


typedef struct NODE
{
    char data[BUFSIZ];                                          // 数据
    struct NODE* next;                                          // 节点指针
} Node;

Node* headNode = NULL;  // 头节点
Node* endNode = NULL;   // 尾节点
Node* second_headNode = NULL; //指向头结点，即顺序第二个结点的指针

void List_Init(void);                                           //链表头结点的初始化
Node* CreateNode(char data[]);                                  // 创建节点
void AddNode(char data[]);                                      // 增加节点
uint GetListSize(void);                                         // 获取大小
void ReadList(void);                                            // 读取链表
void DeleteList(void);                                          // 删除链表
void dofilestruct(Node *nodetemp);                                // 读取链表函数


void print_json(void)
{
    Node *PNode=headNode->next;

	while(PNode!=NULL)
    {
     dofilestruct(PNode);
     PNode=PNode->next;
    }
}


void List_Init(void)
{
    char DATA[100] = "This is the first node,Save useless information !!";
    headNode = CreateNode(DATA);
    endNode = headNode;
}


Node* CreateNode(char Data[])
{

    Node* node = (Node*)malloc(sizeof(Node));
    int i;
    for (i = 0; Data[i] != '\0'; i++)
    {
        node->data[i] = Data[i];
    }
    node->data[i] = '\0';
    node->next = NULL;
    return node;
}


void AddNode(char DATA[])
{
    static char add_count = 0;
    Node* node = CreateNode(DATA);

    if (headNode == NULL)   //链表为空
    {
        headNode = node;
    }
    else  //链表已存在节点
    {
        endNode->next = node;
        add_count++;
    }

    // 把第二个结点的信息传递给second_headNode指针，因为已经创建了一次
    if (add_count == 1)
    {
        second_headNode = node;
    }

    endNode = node;
}


void DeleteList(void)
{
    Node* node = headNode;

    if (node == NULL)
    {
        printf("链表为空！\n");
        return;
    }

    while (node != NULL)
    {
        Node* temp = node;
        node = node->next;
        free(temp);
    }

    headNode = NULL;
    endNode = NULL;
    second_headNode = NULL;

    printf("%s: The LINKED LIST has cleaned.\n", __FUNCTION__);
}


uint GetListSize(void)
{
    Node* node = headNode;
    uint size = 0;

    while (node != NULL)
    {
        node = node->next;
        size++;
    }

    return size;
}


void ReadList(void)
{
    int index;
    Node* node = second_headNode;
    if (node == NULL)
    {
        printf("链表为空！\n");
    }
    else
    {
        for ( index = 1; index < GetListSize(); index++) //index=1，因为跳过了第一个头结点
        {
            printf("%s: Node%d's data = %s\n", __FUNCTION__, index, node->data);
            node = node->next;
        }
        printf("\n");
    }
}


//create a object from struct
int struct_to_cJSON(char *json_string, Node *nodetemp)
{

	if((json_string==NULL) || (nodetemp==NULL))
	{
		printf("%s: input is invalid",__func__);
	}

	char * out=NULL;
	cJSON *root=cJSON_CreateObject();

	if (!root)
	{
		printf("Error before: [%s]\n",cJSON_GetErrorPtr());
		return -1;
	}
	else
	{
		cJSON *obj_node=cJSON_CreateObject();

		cJSON *item=cJSON_CreateString(nodetemp->data);
		cJSON_AddItemToObject(obj_node,"data",item);

		cJSON_AddItemToObject(root,"DATA",obj_node);

		out=cJSON_Print(root);
		printf("out:%s\n",out);

		cJSON_Delete(root);
		if(out!=NULL)
		{
		    strcpy(json_string,out);
			//memcpy(json_string,out,strlen(out));
			free(out);
		}
	}

	return 0;
}

// 关键产生bug的地方
void dofilestruct(Node *nodetemp)
{
    static char file_cout = 0;
    char data[1024];

    /*
    fseek(f,0,SEEK_END);
    len=ftell(f);
    fseek(f,0,SEEK_SET);
    */
    file_cout ++;

    if (1 == file_cout)
        fseek(vvp_out,0,SEEK_SET);

    struct_to_cJSON(data,nodetemp);

    fwrite(data,1,strlen(data),vvp_out);
    fwrite("\n",1,strlen("\n"),vvp_out);
}

int
fprintf_selfmade(FILE* stream, const char* format, ...)
{

	setbuf(stream, outbuf);		//把数据流传入outbuf当中去,给文件指针分配

	va_list arg;
	int done;

	va_start(arg, format);
	done = vfprintf(stream, format, arg);
	va_end(arg);


	// 此处调用生成一个新的链接的节点
	AddNode(outbuf);

	return done;
}


void fputc_selfmade(int ch)
{
    fputc_outbuf[0]= ch;
    fputc_outbuf[1] = '\0';
    AddNode(fputc_outbuf);
}
