""" 
Author: Luo Yusang
"""

import numpy as np


class LogicState:
    B0 = 0 # Binary 0
    B1 = 1 # Binary 1
    BZ = 2 # Binary High Z
    BX = 3 # Binary X   


class GateSimulator:
    nodes = {}
    gates = {}
    stimulation = {}
    upsampled_stimulation = {}
    result = {}
    sim_upsample_ratio = 1
    def load_gates(self,gates):
        pass
    def load_nodes(self,nodes):
        pass
    def load_stimulation(self,stimulation):
        pass
    def load_sim_file(self,filename):

        pass
    def linear_sim(self):
        """
        Simulate Linearly (uniform time-axis)
        """
        time_length = len(self.upsampled_stimulation)
        # Generate space and pre-load driving waveforms
        for node_name, node in  self.nodes.items():
            if node["type"]=="input":
                self.result[node_name] = np.array([time[node_name] for time in self.upsampled_stimulation],dtype=np.uint8)
            else:
                self.result[node_name] = np.full(time_length,255, dtype=np.uint8)
        for time in range(time_length): # driving wave is up sampled
            for name,gate in self.gates.items():
                input_timepoint = time-gate["delay"] if time-gate["delay"]>=0 else 0
                out = self.apply_gate(
                    gate["type"], 
                    [ self.result[in_netname][input_timepoint] for in_netname in gate["inputs"]]
                )
                for out_netname in gate["outputs"]:
                    self.result[out_netname][time]=out
        # return self.result
    
    def apply_gate(self, gate_type, input_list):
        """
        Apply signals of a single timepoint to a single gate.
        """
        if gate_type == "and":
            out = 1
            for input_value in input_list:
                out = out and input_value
            return out
        elif gate_type == "nand":
            out = 1
            for input_value in input_list:
                out = out and input_value
            return not out
        elif gate_type == "or":
            out = 0
            for input_value in input_list:
                out = out or input_value
            return out
        elif gate_type == "nor":
            out = 0
            for input_value in input_list:
                out = out or input_value
            return not out
        elif gate_type == "xor":
            out = 0
            for input_value in input_list:
                out = out +1 if input_value == 1 else out
            return out%2
        else:
            raise Exception("Gate Not Supported.")

    def upsample_stimulation(self):
        """
        Upssample a set of user-defined wave.
        """
        if type(self.stimulation) == list: # list of time-point dicts
            out_list = []
            for values_dict in self.stimulation:
                for _ in range(self.sim_upsample_ratio):
                    out_list.append(values_dict)
            self.upsampled_stimulation = out_list
        elif type(self.stimulation) == dict: # dict of single-node signal lists
            for node_name, node_wave in self.stimulation.items():
                pass
        
    def verify(self):
        # FIXME: need to be completed
        # check if there is any duplication in out list
        # check if any input is driven by gate
        # check if in/out node names are asserted
        # check if driving waveforms are asserted
        # check if delay is zero(delay can not be zero here)
        pass
    def print_wave(self, node_names_list=None,start = 0,stop=None):
        if node_names_list == None:
            node_names_list=list(self.nodes.keys())
        if stop == None:
            stop=len(self.result[node_names_list[0]])
        print("Printing simulate result from time {start} to {stop}:".format(start=start,stop=stop))
        for node_name in node_names_list:
            node_wave = self.result[node_name]
            if self.nodes[node_name]["type"] == "internal":
                marker = "="
            elif self.nodes[node_name]["type"] == "output":
                marker = "<"
            elif self.nodes[node_name]["type"] == "input":
                marker = ">"
            print(marker,node_name,"\t:",end="")
            for time_index in range(start,stop):
                print_val = "\u203E" if node_wave[time_index]==1 else "\u005f"
                print(print_val,end="")
            print("\n")


if __name__ == "__main__":
    # input: need to be driven
    # output: put to results
    # internal: no recording


    import json, argparse
    arg_parser = argparse.ArgumentParser(description = "Gate-Level Simulator")
    arg_parser.add_argument('--json', '-j', default = "sim_data/jk_ff.json", help = 'Json Filename')
    # arg_parser.add_argument('--out', '-o', default = "out.csv", help = 'Out Filename')

    args = arg_parser.parse_args()
    print(args)

    json_file = open(args.json,encoding='utf-8')
    json_dict = json.load(json_file)
    simulator = GateSimulator()


    simulator.stimulation=json_dict["driving_wave"]
    simulator.nodes=json_dict["nodes"]
    simulator.gates = json_dict["gates"]
    simulator.sim_upsample_ratio = json_dict["sim_upsample_ratio"]
    #upsampled_wave = simulator.upsample(json_dict["driving_wave"],json_dict["sim_upsample_ratio"])
    #result=simulator.linear_sim(json_dict["gates"], json_dict["nodes"], upsampled_wave)
    # simulator.print_wave(result,["j","k","cp","g3o","g4o","q","qn"],start = 50)
    simulator.upsample_stimulation()
    simulator.linear_sim()
    simulator.print_wave()



