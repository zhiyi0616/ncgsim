# 使用 SMIC13 库综合并输出反标文件
+ [库文件](https://pan.baidu.com/s/1oCaRSjdrtQtnl6y1xR_kMg)提取码：mkxi 
+ 例代码 [count16](https://github.com/shally520/summer-test)
+ 综合
    1. 新建文件夹 count16 ，将 count16.v 和 count16_tb.v 放进去文件夹里，将文件传入终端
    2. 库文件解压，传入终端中
    3. 综合
        - 确定当前路径在文件 count16 下
        - `design_vision` 打开设计界面
        - 菜单栏 File &rarr; Read &rarr; count16.v 读入设计
        - 设置库如图
            ![库设置](doc_imgs\lib.png)
            + 这里的 Search path 设置为库文件 smic13_tt.db 的路径，这里我把库传入了 DC_2013.12 文件下，所以路径就设为 `/home/designer/DC_2013.12/smic13/STD/Synopsys` 
        - `compile` 综合
        - `change_name -hier -rule verilog` 由于输出网表中有 `\` 等字符，需要加这条命令，规范语法
        - `write -format verilog -hier -o count16_dc.v` 输出网表文件
        - `write_sdf -version 2.1 count16.sdf` 输出sdf文件
    4. 得到的文件有 count16_dc.v 和 count16.sdf 



# VCS 综合后仿真
## VCS 入门参考
 https://www.cnblogs.com/blue163/p/4863859.html

## 命令格式
```shell
vcs -full64 -gui -R XXX.v
```
注意，64位版本要加 `-full64`

## 解决GUI界面乱码
虽然 DC 和 DVE 界面类似，但是在 XServer 相同时，可能 DC 不乱码，DVE 乱码。乱码是字体问题，参考：http://bbs.eetop.cn/thread-618600-1-1.html

对于 xming 下的乱码，安装 https://sourceforge.net/projects/xming/files/Xming-fonts/7.7.0.10/ 即可

1. 仿真所需要的文件，包括 count16_tb.v , count16_dc.v , count16.sdf smic13.v 如图:
    ![文件](doc_imgs\works.png)
2. 在 tb 文件中加入一段 initial 反标 sdf 文件:
    ```
    //反标
    initial
    begin
    $sdf_annotate("count16.sdf",count16);  
    end
    ```
    ***.sdf 是延时文件的文件名，后面是 tb 文件中实例源文件时起的名字。
3. 将三个 .v 文件使用 VCS 编译
    ```
    vcs -full64 *.v -top count16_tb -debug_all -R -gui -timescale=1ns/1ps -negdelay +neg_tchk  -l run.log 
    ```
    -top 设置成 tb 文件的顶层模块，不知何原因会报错 “timescale是非法的” ，在编译设置中加上 -timescale=1ns/1ps 即可编译成功。

4. 反标成功
    ![DVE](doc_imgs\dve.png)
    - 反标完成会显示这样一句话 "Doing SDF annotation......Done"。