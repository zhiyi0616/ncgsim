# 四态仿真，10000000与非门，时间比较
import torch,time



if __name__ == "__main__":
    gate_num_total = 100000000
    gate_input_count = 6
    program_start_time = time.time()
    step_count = 200
    use_cuda = False
    steps_per_time_print = 10
    print("Testing ")

    # Generate Test data
    # One Byte for one gate-output
    step_start_time = time.time()
    test_input_tensor = torch.randint(high=3,dtype=torch.uint8,size = (gate_num_total,gate_input_count)) # 6-input gates
    print("Time used for generating:",time.time()-step_start_time )
    # now one row for inputs of one gate, one column for same input port of all gates.
    # print("Generated Test Data:",test_input_tensor)

    
    if use_cuda:
        # Put tensor to gpu
        step_start_time = time.time()
        test_input_tensor = test_input_tensor.cuda() 
        print("Time used for putting to GPU:",time.time()-step_start_time )

    step_start_time = time.time()
    test_input_tensor = test_input_tensor.t().contiguous()
    # Now one row for same input port of all gates, one column for inputs of one gate.
    print("Time used for transpositioning",time.time()-step_start_time )
    print("Transpositioned",test_input_tensor)
    
    # time.sleep(15)


    result1 = torch.zeros(gate_num_total,dtype=torch.uint8) # Container
    result2 = torch.zeros(gate_num_total,dtype=torch.uint8) # Container
    if use_cuda:
        # Put tensors to gpu
        result1 = result1.cuda() 
        result2 = result2.cuda() 
    
    steps_start_time = time.time()
    in_step_start_time = time.time()
    for step in range(step_count):
        for row in test_input_tensor:
            result1.bitwise_or_(row)
            result2.bitwise_and_(row)
            # result = result ^ row
        if not step%steps_per_time_print:
            print("    [IN-LOOP]Time used for {steps_per_time_print} step(s)".format(
                steps_per_time_print=steps_per_time_print
                ),
                time.time()-in_step_start_time
            )
            in_step_start_time = time.time()
            if use_cuda:
                print("        CUDA Mem Active-Allocated:",torch.cuda.memory_stats()["active.all.allocated"]) # https://pytorch.org/docs/stable/notes/cuda.html#cuda-memory-management
                print("        CUDA Mem Active-Current:",torch.cuda.memory_stats()["active.all.current"])
                print("        CUDA Mem Active-Freed:",torch.cuda.memory_stats()["active.all.freed"])
                print("        CUDA Mem Segment-Allocated:",torch.cuda.memory_stats()["segment.all.allocated"]) # https://pytorch.org/docs/stable/cuda.html#torch.cuda.memory_stats
                print("        CUDA Mem RsvdBytes-Allocated:",torch.cuda.memory_stats()["reserved_bytes.all.allocated"])
    
    #result=result.t() # single-column result
    print(result1, result2)
    print("Time used for {step_count} steps".format(step_count=step_count),time.time()-steps_start_time )
    print("PROGRAM TIME TOTAL:", time.time()-program_start_time)