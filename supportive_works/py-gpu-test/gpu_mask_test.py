import torch

# history_tensor = torch.

a = torch.ByteTensor([
    [1,  2,  3,  4,  5],
    [6,  7,  8,  9,  10],
    [11, 12, 13, 14, 15]
])

b = a.index_select(0, torch.LongTensor([0, 2]))
print(a, b)
