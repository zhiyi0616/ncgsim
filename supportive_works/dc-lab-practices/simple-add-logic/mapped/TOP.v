/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Ultra(TM) in wire load mode
// Version   : O-2018.06-SP1
// Date      : Tue Sep 29 05:31:05 2020
/////////////////////////////////////////////////////////////


module test_add ( a, b, c );
  input a, b;
  output c;


  xr02d1 U2 ( .A1(b), .A2(a), .Z(c) );
endmodule

