// This file is written by Luo Yusang
// include testbench and lib

//Testbench
`timescale 1ps/1ps
module top_tb();
wire out_tb;
reg a_tb;
reg b_tb;
initial begin
    $dumpfile("TOP_tb.vcd");
    $dumpvars(0,top_tb);
    a_tb = 0;
    b_tb = 0;
    #10 a_tb = 1;
    #6  a_tb = 0;
    //#2  b_tb = 1;
    //#4  b_tb = 0;
    #100 ;
end
test_add dut1(a_tb,b_tb,out_tb);
endmodule


// lib
module xr02d1(A1,A2,Z);
    input wire A1,A2;
    output wire Z;
    assign Z = A1+A2;
endmodule
