# DC Lab Practices

# Lab 工程文件
1. 下载工程文件,下载地址: [Github](https://github.com/shally520/summer-test/tree/master/lab1)
2. 在远程主机（虚拟机）中安装 Eda 工具
3. 在个人电脑中下载 Bitvise SSH Client ,连接远程主机
4. 需要在个人电脑中下载 Xming 才能在终端中连接 Design Vision 界面，下载地址: [Xming](https://sourceforge.net/projects/xming/files/Xming-fonts/7.7.0.10/)
5. 将 Bitvise SSH Client 菜单栏中 Terminal 下 X11 Forwarding Enable 前方框勾选，启动虚拟机后，双击 Xming，后台显示运行，在启动 Design Vision 时窗口才能弹出，否则将无法连接 Design Vision 
   - 如图所示
      ![x11映射](doc_imgs\Xming.png)
6. 把工程文件传入个人电脑的虚拟机 `designer` 目录下
# Lab Flow
## 检查并修改 Tcl 文件
1. 将 `lab1` 作为当前目录 `cd lab1` （lab1不在designer路径下，需要一层一层进）
2. 列出 `lab1` 下所有文件 `ls` ，确保文件齐全
3. 补充 `common_setup.tcl` 空白处,通过命令 `gedit common_setup.tcl` 
   ```
   set ADDITIONAL_SEARCH_PATH        "../ref/libs/mw_lib/sc/LM ./rtl ./scripts";  
   set TARGET_LIBRARY_FILES          sc_max.db;
   set SYMBOL_LIBRARY_FILES          sc.sdb;
   set MW_DESIGN_LIB                 TOP_LIB;
   set MW_REFERENCE_LIB_DIRS         ../ref/libs/mw_lib/sc;
   set TECH_FILE                     ../ref/libs/tech/cb13_6m.tf  ;
   set TLUPLUS_MAX_FILE              ../ref/libs/tlup/cb13_6m_max.tluplus  ;
   set TLUPLUS_MIN_FILE              ../ref/libs/tlup/cb13_6m_min.tluplus  ;
   set MAP_FILE                      ../ref/libs/tlup/cb13_6m.map  ;
   ```
4. 遇到的问题
   - 填写 *common_setup.tcl* 文件时，路径之间需要用空格间隔开
## 调用 Design Vision 
1. 查看当前路径是否在 `lab1` 下 `pwd` 
2. 调出 Design Vision 界面 `design_vision -topo`
3. 在 Design Vision 界面菜单栏中选择 *File &rarr; Setup* 
   - 确保路径正确
      ![路径设置](doc_imgs\path.png)
4. 检查 *Logical and Physical libraries* 是否一致 `check_library` 
5. 检查 *TLUPus and Technology* 文件是否一致 `check_tlu_plus_files` 
6. 以上没有出错时会出现 passed 
## 读入设计
1. 点击菜单栏中 *File &rarr; Read*
   - 点击当前目录下 `rtl` 文件，选择 `TOP.v` 文件，点击 Open ，设计便以读入
   - 选中 hier1 窗口中的设计 TOP 确保与右下角窗口显示 *Design:TOP* 一致
2. 选择菜单栏中 *File &rarr; Link Design &rarr; OK* 
   - 链接 Link library ，解析所有引用
   - 在 *log Area* 不应该出现警告或者错误
3. 将当前设计以 ddc 的文件格式保存在 `lab1/unmapped` 中(这里的文件格式也可以换成 verilog )
   ```
   write -hier -f ddc -out unmapped/TOP.ddc
   ```
4. 在命令行输入以下命令，可以查看在 Memory 中设计和库列表
   ```
   list_designs
   list_libs
   ```
## 分析设计
- 选中 *Logical Hierarchy* 窗口中的 *TOP*
- 点击菜单栏中 *Schematic &rarr; New Schematic Views*
- 将弹出一个新的窗口 Schematic.1
## 约束设计
- 约束 *TOP* `source TOP.con` 
## 编译
- 在 *design vision* 命令框输入命令 `compile_ultra` 
## 保存设计
1. 菜单栏中 *File &rarr; Save As*
2. 双击 `mapped` 
3. 输入 `TOP.v` 在文件名字中
4. 确保 Save all designs in hierararchy 被选择
5. 点击 Save ,综合的门级网表便以保存在 `mapped` 目录下
## 删除设计并退出***Design Vision***
1. 执行以下命令删除所有历史命令 `fr`
2. 记录历史命令 `h`
   - 执行这个命令所有历史命令将自动记录在创建在 `lab1` 目录下的 `command.log` 文件中。
3. 退出 *Design Vision* ,在命令行输入 `exit`
## 另一种调用 ***Design Vision*** 的方法
1. 在 linux shell 界面输入：
   ```
   dc_shell -topo
   ```
2. 在 DC_shell 界面输入
   ```
   design_vision -topo> start_gui
   ```
3. 退出 DC_shell
   ```
   design_vision -topo> stop_gui
   ```




