

# GitLab 搭建

Author: 胡海川

[toc]

## 环境准备

服务器参数如下：

- OS：CentOS Linux release 7.5.1804 (Core)
- 内存：16GB
- CPU型号：Intel(R) Xeon(R) CPU E5-2620 v3 @ 2.40GHz
- CPU核数：6
- 硬盘大小：13989.5 GB
- 内网IP：192.168.8.2

## 基本搭建步骤

1.安装一些依赖软件包

```bash
$ sudo yum install -y curl policycoreutils-python openssh-server
$ sudo systemctl enable sshd
$ sudo systemctl start sshd
```

2.关闭防火墙，或者开放HTTP的端口

```bash
//刷新防火墙的规则
$ iptables -F
```

3.安装邮件服务

```bash
$ sudo yum install postfix
$ sudo systemctl enable postfix
$ sudo systemctl start postfix
```

4.从官网获取一键安装脚本

```bash
$curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh > rpm.sh
$chmod +x rpm.sh
$./rpm.sh
```

5.安装gitlab

```bash
//使用yum安装gitlab
$yum install -y gitlab-ee
//可以看下gitlab-ee包的内容，看到gitlab安装在/opt/gitlab目录下
$rpm -ql gitlab-ee | less
```

6.修改gitlab配置文件指定服务器ip和自定义端口

```bash
$vim  /etc/gitlab/gitlab.rb
```

默认是8080端口

7.重置并启动gitLab

```bash
$gitlab-ctl reconfigure
$gitlab-ctl restart
```

自此本地服务已经配好，可以通过内网访问192.168.8.2:8080

## Nginx反向代理

我们需要外网能够访问gitlab，但由于登录节点端口紧张，我们采取nginx反向代理技术来解决这个问题。

- 目标服务器：192.168.8.2
- 代理服务器：222.204.6.192
- 访问域名：https://good.ncu.edu.cn/gitlab

1.首先编辑gitlab配置文件

```bash
$ vim /etc/gitlab/gitlab.rb

# 让gitlab的内置nginx监听7000端口
nginx['listen_port'] = 7000

# 设置gitlab的访问路径（是通过外部nginx反向代理访问的）
external_url 'http://192.168.8.2/gitlab'

```

2.让配置生效

```bash
# 让配置生效
$ gitlab-ctl reconfigure
$ gitlab-ctl restart
```

3.配置外部登录节点nginx

```bash
$ vim /etc/nginx/conf.d/good_web_ssl.conf


 location /gitlab/ {
			# 设置最大允许上传单个的文件大小
              client_max_body_size 512m;
              proxy_redirect off;
              proxy_buffer_size 64k;                                                                                                                    
              proxy_buffers 32 32k;
              proxy_busy_buffers_size 128k;
				#以下确保 gitlab中项目的 url 是域名而不是 http://git，不可缺少
              proxy_set_header Host $host;
              proxy_set_header X-Real-IP $remote_addr;
              proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
 				# 反向代理到 gitlab 内置的 nginx
              proxy_pass http://192.168.8.2/gitlab/;
          }   
```

4.重启nginx

```bash
# 检测配置是否正确
$ nginx -t
# 重启nginx
$ nginx -s reload
```

## 踩坑经历

### 502报错

![502 Error](doc_imgs/gitlab_502.png)


一、首先看看配置文件/etc/gitlab/gitlab.rb 中的端口号是否被占用

当时服务器中在跑许多docker服务，造成了端口冲突。

二、另外一个原因是gitlab占用内存太多，导致服务器崩溃

解决办法可以启用swap分区

步骤如下： 

1.查看swap分区是否启动

```bash
$ cat /proc/swaps 
```

2.创建swap大小为bs*count=4294971392(4G)

```bash
$ dd if=/dev/zero of=/data/swap bs=512 count=8388616 
```

3.通过mkswap命令将上面新建出的文件做成swap分区

```bash
$ mkswap /data/swap 
```

4.查看内核参数vm.swappiness中的数值是否为0，如果为0则根据实际需要调整成60 

```bash
$ cat /proc/sys/vm/swappiness
```

 设置：sysctl -w vm.swappiness=60 

若想永久修改，则编辑/etc/sysctl.conf文件，改文件中有vm.swappiness变量配置，默认为0 

5.启用分区 

```bash
$ swapon /data/swap echo “/data/swap swap swap defaults 0 0” >> /etc/fstab 
```

6.再次使用cat /proc/swaps 查看swap分区是否启动 

7.重启gitlab。

### 更改gitlab克隆地址

安装好Gitlab，创建项目之后，我就把自己的代码推送到了Gitlab上，但是当别的开发人员去克隆的时候，发现项目中的克隆地址是这样的，仍然是代理服务器在内网的地址：

![克隆地址错误](doc_imgs/gitlab_clone_addr_err.png)

SSH和HTTP的克隆地址都为https://good.ncu.edu.cn/gitlab。虽然我可以把这个地址复制下来然后在修改成自己的IP地址，但是这样真实有点不方便。

解决方法：

1.修改gitlab的配置文件

```bash
$ vim /opt/gitlab/embedded/service/gitlab-rails/config/gitlab.yml
```

![设置](doc_imgs/gitlab_clone_addr_settings.png)

把host后面改为自己的IP或者域名，如果Gitlab的http端口不是80端口，还可以对端口进行修改

2.修改完后，重启gitlab：

```bash
$ gitlab-ctl restart
```

