# Install VMware
VMware Workstation Pro15.x

## Keys From CNBlogs
https://www.cnblogs.com/liuqun/p/11737327.html
```
YG5H2-ANZ0H-M8ERY-TXZZZ-YKRV8
UG5J2-0ME12-M89WY-NPWXX-WQH88
UA5DR-2ZD4H-089FY-6YQ5T-YPRX6
GA590-86Y05-4806Y-X4PEE-ZV8E0
ZF582-0NW5N-H8D2P-0XZEE-Z22VA
YA18K-0WY8P-H85DY-L4NZG-X7RAD
```


## Keys From Baidu
```
ZKKZC-XMEVN-C1QFP-AYJE3-7Z5AV
V99NW-THRL7-01YR9-MJ79T-UUEZV
7NVYU-K89KZ-T1P9K-Y4PQW-PYGEV
ZRLPF-GHFYE-E1VYV-XVUEZ-LL5VZ
YRVYY-T0742-71PPG-2FRZ5-4QXNV
GTZMF-ZUDYG-F1Y7W-WUZCY-LVQEZ
ZZQME-VDX2T-X1PTL-2YMT1-5PN3V
XG0NZ-XKGF1-V1QRZ-GAF9V-ZCQRV
URZMW-TW1K5-51YKC-UMRC5-6ZQEV
Z2YWT-V8XLG-71ZWG-AR89Q-QUEWZ
```

# Move/Setup VM
- Unzip `RHELEDA.zip` to a local path.
- Open `RHELEDA.vmx` file via VMware installed.
- Start and click "I Have Moved" button when see prompt.
- Note that you should choose "I Have Moved", not "I Have Copied".
- Note that in same LAN, this VM should only be deployed once. Or MAC conflict will occur.


# For the EDA VM Installed in VMware
Here are some internal setups(specified) in the EDA server in A212:
- Password of user `root` and `designer` has been changed from `123456` to `NCUEDA0147896325`
- Note that `slm` is an alias.
- Set network adapter to bridge mode outside the VM 
- Enable DHCP inside VM: `vi /etc/sysconfig/network-scripts/ifcfg-ens33` and set `ONBOOT` to `yes`



# SSH and X11 Redirection
- Use Bitvise or X-Shell/FinalShell/PuTTY/Terminus/MobaXterm as SSH Client.
- If you are using Windows, You may use Xming to access GUI. Settings should be specified in the SSH client you use.


# Start License Manager
RUN at linux end:
```shell
slm
```
Nothing will happen if `slm` alias command ran successfully.

# Startup DC(Design Compiler)
https://wenku.baidu.com/view/e9257f8527284b73f3425001.html#

Start GUI:
```shell
design_vision
```
or:
```shell
dc_shell -gui
```


