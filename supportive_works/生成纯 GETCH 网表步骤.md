# 生成纯 GETCH 网表步骤
## 前期准备
+ 16位乘法器和32位加法器.v文件，替换 `lab1` 中 `rtl/TOP.v` ,对 `lab1` 重命名，以16位乘法器为例，命名为 `mul` 
+ 由于在终端虚拟机中 /tools/Synopsys/syn_vO-2018.06-SP1/syn/O-2018.06-SP1/libraries/syn 路径下有 `gtech.db` 库，所以我们在设计库时可以直接调用
## 基本步骤
+ 先检查当前路径是否在 `mul` 中 `pwd` 
+ 然后在连接 Design Vision 时使用命令 `design_vision` 
+ 菜单栏中选择 File &rarr; Setup ,对 Link library 和 Target library 重新设置
   - 如图所示
       ![库设置](doc_imgs/path.png)
+ 对于接下来的步骤做了以下四种尝试
   1. Test one
       - 先读取设计 `read_file rtl/multiplier.v`
       - 然后直接输出网表 `write -f verilog -o mul.v` 
       - 输出的网表中包含 GTECH， ADD_UNS_OP 和 ELECT_OP ，不符合我们要求的纯 GTECH
   2. Test two
       - 先读取设计 `read_file rtl/multiplier.v`
       - 进行编译 `compile`
       - 然后输出网表 `write -f verilog -o mul.v`
       - 输出的网表是纯的 GTECH 网表，符合我们的要求
   3. Test three
       - 先 `elaborate multiplie`
       - 输出网表 `write -f verilog -o mul.v`
       - 得到的网表和第一种的一样
    4. Test four
       - 先读取设计 `read_file rtl/multiplier.v`
       - 再 `elaborate multiplie`
       - 进行编译 `compile`
       - 然后输出网表 `write -f verilog -o mul.v`
       - 输出的是纯的 GTECH 网表，我们也得到一个结论， `elaborate multiplie` 是不必要的的一步
+ 总结:步骤基本包含三步，读入设计 `read_file rtl/multiplier.v` ,编译 `compile` ，输出文件 `write -f verilog -o mul.v` 