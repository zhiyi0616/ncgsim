# iVerilog VVP 事件的抛出和执行
- 事件类型： 详见 [事件类型](./iVerilog%20VVP%20事件类型.md)

# 调度的目标
- SEQ_START
- SEQ_ACTIVE
- SEQ_INACTIVE
- SEQ_NBASSIGN
- SEQ_RWSYNC
- SEQ_ROSYNC
- DEL_THREAD


# Schedule 函数
VVP 使用以 `schedule_<XXX>` 作为函数名的函数进行事件的抛出。这些函数大多声明在 `schedule.h` 中。

| 调度函数                     | 调度目标队列       | 说明                                             |
| ---------------------------- | ------------------ | ------------------------------------------------ |
| schedule_vthread             | SEQ_ACTIVE or push |                                                  |
| schedule_inactive            | SEQ_INACTIVE       |                                                  |
| schedule_init_vthread        |                    |                                                  |
| schedule_final_vthread       |                    |                                                  |
| schedule_assign_vector       | SEQ_NBASSIGN       | 生成向量赋值事件，主要用于实现非阻塞赋值         |
| schedule_assign_array_word   | SEQ_NBASSIGN       |                                                  |
| schedule_force_vector        | SEQ_NBASSIGN       |                                                  |
| schedule_propagate_vector    | SEQ_NBASSIGN       |                                                  |
| schedule_set_vector          | SEQ_ACTIVE         | 类似 schedule_assign_vector 但是生成 active 事件 |
| schedule_t0_trigger          | SEQ_INACTIVE       | 生成 T0 事件                                     |
| schedule_init_vector         |                    | 生成仿真前初始化事件                             |
| schedule_init_propagate      |                    | 生成仿真前传播事件，不经过 net functor           |
| schedule_generic             | depends            | 生成一般事件                                     |
| schedule_functor             | SEQ_ACTIVE         | 生成 functor 输出事件                            |
| schedule_at_start_of_simtime |                    |                                                  |
| schedule_at_end_of_simtime   |                    |                                                  |
| schedule_del_thr             | DEL_THREAD         | 生成线程删除事件到 Readonly-sync 之后            |


以上函数均调用四个函数中的一个或几个：
- schedule_event_
- schedule_init_event
- schedule_final_event
- schedule_event_push_ 调度到任何事件之前




## 具有 Delay 的 Schedule 函数
以下调度函数具有 Delay ：
- schedule_vthread
- schedule_generic
- schedule_at_start_of_simtime
- schedule_at_end_of_simtime

## Schedule 函数的调用位置

文档待完善


