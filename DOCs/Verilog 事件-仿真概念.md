# Verilog 分层事件队列

- 当前事件
  - Active
    - 阻塞赋值（右边式计算和更新）
    - 非阻塞赋值的右边式计算
    - 连续赋值
    - `$display` 语句执行
    - 计算原语输入，改变输出
  - Inactive
    - `#0` 延迟的阻塞赋值
    - PLI Callback
  - NBA 非阻塞赋值更新
    - Active 区计算好的放到左边
  - Monitor 监控
    - `$monitor`
    - `$strobe`
- 将来事件
  - 被调度到将来的事件

# 赋值
连续赋值不是过程赋值，用 assign 语句写在 always 、 initial 等块之外。而阻塞和非阻塞是过程赋值，其被赋值的变量只能放在过程里。 always ， initial 这类表示过程的块，在仿真器中就是进程或者线程，彼此相互唤醒和阻塞。三种赋值都是由事件驱动的。



