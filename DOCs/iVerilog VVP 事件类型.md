# 关于事件的问题

## 一个时间点要执行哪几类事件串？
每一类事件队列都定义在结构体 `event_time_s` 中：
```c++
struct event_s*start;
struct event_s*active;
struct event_s*inactive;
struct event_s*nbassign;
struct event_s*rwsync;
struct event_s*rosync;
struct event_s*del_thr;
```


## iVerilog 有哪几类事件？
根据 `/vvp/schedule.cc` 中的定义，`event_s` 的派生类如下：

| Name                      | Description                  |
| ------------------------- | ---------------------------- |
| vthread_event_s           | Create Thread?               |
| del_thr_event_s           |                              |
| assign_vector4_event_s    |                              |
| assign_vector8_event_s    |                              |
| assign_real_event_s       |                              |
| assign_array_word_s       |                              |
| force_vector4_event_s     |                              |
| propagate_vector4_event_s |                              |
| propagate_real_event_s    |                              |
| assign_array_r_word_s     |                              |
| generic_event_s           | Include vvp_gen_event_t obj; |

对应执行函数的大致内容如下：

| Name                      | Major operation in run_run()               |
| ------------------------- | ------------------------------------------ |
| vthread_event_s           | vthread_run(thr)                           |
| del_thr_event_s           | vthread_delete(thr)                        |
| assign_vector4_event_s    | if wide is not zero, vvp_send_vec4_pv(...) |
| assign_vector8_event_s    | vvp_send_vec8(ptr, val);                   |
| assign_real_event_s       | vvp_send_real(ptr, val, 0);                |
| assign_array_word_s       | mem->set_word(adr, off, val);              |
| force_vector4_event_s     | net->force_vec4(tmp, mask);                |
| propagate_vector4_event_s | net->send_vec4(val, 0);                    |
| propagate_real_event_s    | net->send_real(val, 0);                    |
| assign_array_r_word_s     | mem->set_word(adr, val);                   |
| generic_event_s           | obj->run_run();                            |

注意， `vvp_net_t::force_vec8(...)` 函数定义了但是没地方用的。

另外，未定义在 schedule 内的，但是具有 run_run() 方法的类：

| Name                 | In File         |
| -------------------- | --------------- |
| vvp_fun_delay        | delay.cc        |
| vvp_fun_modpath      | delay.cc        |
| evctl_real           | event.cc        |
| evctl_vector         | event.cc        |
| evctl_array          | event.cc        |
| evctl_array_r        | event.cc        |
| vvp_fun_and          | logic.cc        |
| vvp_fun_buf          | logic.cc        |
| vvp_fun_muxr         | logic.cc        |
| vvp_fun_muxz         | logic.cc        |
| vvp_fun_not          | logic.cc        |
| vvp_fun_or           | logic.cc        |
| vvp_fun_xor          | logic.cc        |
| vvp_fun_part_sa      | part.cc         |
| sfunc_core           | sfunc.cc        |
| vvp_udp_fun_core     | udp.cc          |
| sync_cb              | vpi_callback.cc |
| vpip_put_value_event | vpi_priv.cc     |
| vvp_island           | vpi_island.cc   |