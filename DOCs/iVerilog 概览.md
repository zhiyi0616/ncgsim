# 编译过程
1. 用户执行 `autoconf.sh`
   1. `autoconf -f` 处理所有 `.in` 文件。
   2. gpref 生成 lexor
   3. gpref 生成 vhdlpp lexor
2. 用户执行 `./configure` （由 `configure.in` 生成）
3. 用户执行 `make` 或者 `mingw32-make` （读取 `/Makefile` ，`/Makefile` 由 `autoconfig.sh` 生成 ）

# 目录结构
详见 `/developer-quick-start.txt` ，摘要如下：
## 编译器组件
- The compiler driver (driver/) 用户入口，主要提供命令解析。
- The preprocessor (ivlpp/) 预处理器，处理一部分反引号开头的命令。
- The core compiler (this directory) 根目录，仿真核心，编译成 `ivl`
- The loadable code generators (tgt-*/) 文件输出，核心做完词法分析和语义解析后

## 运行时组件
- The vvp runtime (vvp/) 仿真器，目录中还有进一步的文档
- The system tasks implementations (vpi/) PIL-2 系统任务
- The PLI-1 compatibility library (libveriuser/) PLI-1 支持
- The Cadence PLI module compatibility module (cadpli/) Cadence PLI 支持

# 问题
## 编译之前， `./configure` 中的 AC_XXXX 是什么函数？是什么工具定义的？
待解答。

## Verilog 语句中的 `specify` 信息是何时消失的？
待解答。


## vvp 事件中，有哪些是可以抛出新事件的？
待解答。


## iVerilog vvp 用什么方式把事件调度到将来？
待解答。


## net 怎么接收信号？
待解答。









