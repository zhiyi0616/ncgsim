# Code structure docs of iVerilog

​	Icarus Verilog是由Stephen Williams开发的Verilog仿真工具，官网[http://iVerilog.icarus.com](http://iVerilog.icarus.com/)，代码开源在https://github.com/steveicarus/iVerilog。

​	经过调研分析得知，iVerilog开源软件大概分为 **编译、elaboration、仿真 **三个阶段。我们在调研的过程中使用的是MSYS2平台上进行安装编译运行iVerilog。本说明文档将从iVerilog的源码部分解析iVerilog的代码结构和iVerilog的编译过程。iVerilog源码整体目录框图和iVerilog工作流程如下：

- iVerilog源码整体目录框图

  <img src="iVerilog-sourcecode.jpg" style="zoom:67%;" />

  


- iVerilog 运行工作流程 

<img src="iVerilog-complie.jpg" style="zoom:67%;" />

​																					

# 编译

​	编译就是从接收命令行参数开始，到预处理（verilog宏展开，文件include，条件编译），Verilog语法解析（关键字识别、语法解析），最后转换成内部的数据结构（就是用各种class、结构体，如module、net、scope、generate、statement、expression等）的过程。简单的讲，就是识别Verilog文件，转成内部的数据库。经过编译后，各个Verilog文件在库里是独立的。最后编译过程结束以后会生成一个内部的pform数据格式的表格传递给后面的工作进行处理。

**编译的流程框架请参考： “iVerilog 运行工作流程图“ 当中的编译过程部分**



## 编译组件

### 用户接口/驱动器（/driver)

```
driver
	 \ cflexor.lex
   	 \ cfparse_misc.h
     \ cfparse.y
     \ globals.h
     \ iverilg.man.in
   	 \ main.c
   	 \ Mainfile.in
   	 \ substit.c
```



​	编译器驱动程序，这个程序接受命令行参数，并组装所有其他子命令的调用，以执行编译步骤。driver目录是iVerilog的可执行文件的源码。主要功能是接收命令行参数，启动子命令来实现编译。cflexor.lex和cfparse.y两个特殊文件。cflexor.lex是flex的输入文件，定义了命令行参数格式，如+define+、-y、-f等。而cfparse.y是bison的输入文件，类似一个c语言的模板，经过bison处理后，得到完整的parser源码（cfparser.c）；cfparser.c文件可以对用户输入的命令进行语法解析。具体的 cflexor.lex 和 cfparse.y 编译的过程可以详见 /driver/Mainfile.in。

- cflexor.lex 编译的Makefile命令如下：

```makefile
LEX = flex

cflexor.c: $(srcdir)/cflexor.lex
	$(LEX) -s -t $< > $@
```



- cfparse.y 编译的Makefile命令如下：

```makefile
YACC = bison

cfparse%c cfparse%h: $(srcdir)/cfparse%y
	$(YACC) --verbose -t -p cf -d -o cfparse.c $<
```

​	当用户接口/驱动器（/driver) 会生成几个临时文件（temporary file) ,这些文件存储了解析完用户命令信息。（/driver)会把这些临时文件（temporary file)传递给iVerilog当中预处理器（/ivlpp)以及根目录下面的编译器核心。

​	当解析完用户输入命令行参数后，(/driver)会通过system()函数来自动调动（预处理器（ivlpp.exe), 编译器核心（ivl.exe））。system函数的定义形式：`int system(const char *command)`;system()函数在Linux/Unix操作系统下，会调用fork函数产生一个新的子进程。由新产生的子进程来执行command命令,当command命令执行完之后返回原调用函数的进程。调用system()函数部分源码如下：

```c
      if (verbose_flag)
	    printf("preprocess: %s\n", cmd);

      rc = system(cmd);
      remove(source_path);
      free(source_path);
```

​	当开启一个新的子进程之后，会利用进程通信机制当中的管道通信把需要传输的命令文件信息传给开启的子进程。这样就可以实现：用户接口/驱动器（/driver)处理完用户命令之后对 ivlpp.exe、ivl.exe的调用和命令信息的传递。



### 预处理器 (ivlpp/) 【还在深入调研】

```
ivlpp    
		\ globals.h    
		\ lexor.lex    
		\ main.c    
		\ Makefile.in
```

​	ivlpp命令是处理文件包含和宏替换的Verilog预处理器。该程序独立于实际编译器运行，从而减轻了编译器本身的任务，并提供了一种脱机预处理文件的方法。具体ivlpp预处理器的详细使用可以参考源码里面ivlpp文件夹下面的说明文档

- 预处理

​    首先用flex来进行关键字（include|define|undef|ifdef|ifndef|else|elseif|endif）扫描，然后把信息存到struct include_stack_t、ifdef_stack_t、define_t的结构体里，形成文件列表、define列表，然后用函数逐个处理，如包括文件、宏展开。相关的函数定义在ivlpp文件夹下面的'lexor.lex'文件夹当中：

```C
static void  def_start(void);
static void  def_add_arg(void);
static void  def_finish(void);
static void  def_undefine(void);
static void  do_define(void);
static int   def_is_done(void);
static void  def_continue(void);
static int   is_defined(const char*name);

static int   macro_needs_args(const char*name);
static void  macro_start_args(void);
static void  macro_add_to_arg(int is_whitespace);
static void  macro_finish_arg(void);
static void  do_expand(int use_args);
static const char* do_magic(const char*name);
static const char* macro_name(void);

static void  include_filename(void);
static void  do_include(void);

static int   load_next_input(void);
```





### 编译器核心（iVerilog根目录）

​	“ivl”程序是执行所有Verilog编译器处理的核心，这些处理在其他地方无法处理。这是Icarus Verilog编译器的主要核心，而不是运行时。其中的“ivl”二进制文件是核心编译器，它完成了编译Verilog源代码(包括库)和生成输出的繁重工作。这是Icarus Verilog编译系统中最复杂的组件。**在iVerilog根目录下的复杂工作几乎包含了iVerilog对的大部分核心工作，比如对Verilog源码的词法、语法分析、生成pform格式数据、elaboration过程以及netliost form数据格式的转换输出**。下面将会重点阐述在编译流程过程中涉及到的工作组件：

- 词法分析组件

​    对于Verolog源码的词法分析的模块大部分是在根目录下面进行的，词法分析的本质上就是“flex”输入文件“lexor.lex“识别输入流中stream当中的信息，这叫做“词法分析。

> ​	flex是LEXical compiler的缩写，是Unix环境下非常著名的工具,主要功能是生成一个词法分析器(scanner)的C源码,描述规则采用正则表达式(regular expression)。描述词法分析器的文件*.l，经过lex编译后，生成一个lex.yy.c 的文件，然后由C编译器编译生成一个词法分析器。词法分析器，简单来说，其任务就是将输入的各种符号，转化成相应的标识符(token)，转化后的标识符 很容易被后续阶段处理。



​	**词法分析器还对编译器指令进行一些处理，这些指令不由外部预处理器处理**。在词法分析lexor.lex里，解析了Verilog注释（包括注释里的synthesis translate_on/off）、操作符、运算符、各种进制的数字、timescale、UDP、always、initial、敏感列表、Verilog系统函数等，当然还有刚才gpref里定义的关键字。详细的iVerilog编译生成词法分析组件的信息可参考根目录下的Makefile代码：

```Makefile
lexor.cc: $(srcdir)/lexor.lex
	$(LEX) -s -t $< > $@
	
lexor_keyword.o: lexor_keyword.cc parse.h

lexor_keyword.cc: $(srcdir)/lexor_keyword.gperf
	gperf -o -i 7 -C -k 1-4,6,9,$$ -H keyword_hash -N check_identifier -t $(srcdir)/lexor_keyword.gperf > lexor_keyword.cc || (rm -f lexor_keyword.cc ; false)

```



​	如下框图为iVerilog源码编译生成词法分析组件流程框图

<img src="iVerilog-word-analyse.jpg" style="zoom:80%;" />



- 语法分析组件

​    对于Verolog源码的词法分析的模块大部分是在根目录下面进行的，对于语法分析的大概过程在GitHub官网给出的官方文档是这样说明：

>​	The parser input file "parse.y" is passed to the "bison" program to generate the parser. The parser uses the functions in parse*.h,parse*.cc, pform.h, and pform*.cc to generate the pform from the stream of input tokens. The pform is what compiler writers call a "decorated parse tree".

​	在我们进行是深入调研中，为了深入地分析清楚iVerilog语法分析编译时生成过程，我们可以从源码下面的Makefile这个切入点进行切入分析。

```Makefile
YACC = bison

parse%cc parse%h: $(srcdir)/parse%y
	$(YACC) --verbose -t -p VL --defines=parse.h -o parse.cc $<

syn-rules.cc: $(srcdir)/syn-rules.y
	$(YACC) --verbose -t -p syn_ -o $@ $<
```

​	从根目录下面的Makefile可以大概的知道iVerilog编译生成语法分析模块需要的大概文件和编译的流程。parse.y经过bison程序产生parser。parser调用iVerilog根目录下相应的API进行语法分析生成pform格式的数据.在语法分析parse.y里，完成了高级的语法解析。类似把单词（关键字）组成段落（always块），段落组成文章（module）;

对于bison程序的介绍如下：

>Introduction to Bison
>
>		Bison is a general-purpose parser generator that converts an annotated context-free grammar into a deterministic LR or generalized LR (GLR) parser employing LALR(1) parser tables. As an experimental feature, Bison can also generate IELR(1) or canonical LR(1) parser tables. Once you are proficient with Bison, you can use it to develop a wide range of language parsers, from those used in simple desk calculators to complex programming languages.
>		Bison is upward compatible with Yacc: all properly-written Yacc grammars ought to work with Bison with no change. Anyone familiar with Yacc should be able to use Bison with little trouble. You need to be fluent in C or C++ programming in order to use Bison. Java is also supported as an experimental feature.

如下框图为iVerilog源码编译生成语法分析组件流程框图：

![](grammmar-analyse.jpg)



## pform格式的数据

​	pform是经过编译解析后的verilog文件,verilog文件在经过预处理、词法分析、语法分析之后iVerilog用自身的内部数据来进行封装，以便后续能够更加高效地进行处理。对于pform内部数据我们可以在iVerilog源码当中的pform.h头文件中查看pform数据格式的定义

```c++
pform.h file:

/*
 * These classes implement the parsed form (P-form for short) of the
 * original Verilog source. the parser generates the pform for the
 * convenience of later processing steps.
 */

class PGate;
class PExpr;
class PPackage;
class PSpecPath;
class PClass;
class PPackage;
struct vlltype;

```

​	

​	**根据源码我们可以知道在iVerilog当中就是一堆C++当中的类和struct来进行数据封装，这些类和结构体包含了 verilog当中的基本门电路、表达式、任务、函数、UDP、wire、verilog class、generate、package、modport、scope等**。为了更好源码阅读在定义这些class类的时候，都统一在类的首字母采用大写的P。后续的elaboration阶段会直接采用这些封装好的数据即pform内部数据形式进行处理。



## elaboration （调研中）





## netlist form

​	netlist form在经过elaboration过程后生成，然后经过一些优化和处理之后被翻译成ivl target form数据形式。最后，通过ivl_rarget.h当中的API将ivl target form 内部数据传递给代码生成器。具体的传送形式如下：

   如下的代码是ivl_target.h当中定义的众多结构体指针。当iVerilog经过编译，elaboration过程之后生成了内部的数据形式netlist form形式数据。然后这些数据会被翻译成ivl target form内部数据形式封装在众多的结构体当中 。然后为了引用这些结构体当中的内部信息，为此在头文件当中给每一个结构体重命名了众多的结构体指针。通过结构体指针来指引读取这些封装在结构体当中的信息。

```C
typedef struct ivl_array_s    *ivl_array_t;
typedef struct ivl_branch_s   *ivl_branch_t;
typedef struct ivl_delaypath_s*ivl_delaypath_t;
typedef struct ivl_design_s   *ivl_design_t;
typedef _CLASS ivl_discipline_s*ivl_discipline_t;
typedef const _CLASS netenum_t*ivl_enumtype_t;
typedef struct ivl_event_s    *ivl_event_t;
typedef struct ivl_expr_s     *ivl_expr_t;
typedef struct ivl_island_s   *ivl_island_t;
typedef struct ivl_lpm_s      *ivl_lpm_t;
typedef struct ivl_lval_s     *ivl_lval_t;
typedef struct ivl_net_const_s*ivl_net_const_t;
typedef struct ivl_net_logic_s*ivl_net_logic_t;
typedef struct ivl_udp_s      *ivl_udp_t;
typedef _CLASS ivl_nature_s   *ivl_nature_t;
typedef struct ivl_net_probe_s*ivl_net_probe_t;
typedef struct ivl_nexus_s    *ivl_nexus_t;
typedef struct ivl_nexus_ptr_s*ivl_nexus_ptr_t;
typedef struct ivl_parameter_s*ivl_parameter_t;
typedef struct ivl_process_s  *ivl_process_t;
typedef struct ivl_scope_s    *ivl_scope_t;
typedef struct ivl_signal_s   *ivl_signal_t;
typedef struct ivl_port_info_s*ivl_port_info_t;
typedef struct ivl_switch_s   *ivl_switch_t;
typedef struct ivl_memory_s   *ivl_memory_t; //XXXX __attribute__((deprecated));
typedef struct ivl_statement_s*ivl_statement_t;
typedef const _CLASS ivl_type_s*ivl_type_t;`
```

​	例如其中的 `typedef struct ivl_scope_s    *ivl_scope_t;`就重新定义了一个结构体指针的新类型，利用结构体指针指引`ivl_scope_s`当中的信息。改结构体存储的内部数据信息如下：

```C
struct ivl_scope_s {
      ivl_scope_s();

      ivl_scope_t parent;
      std::map<hname_t,ivl_scope_t> children;
	// This is just like the children map above, but in vector
	// form for convenient access.
      std::vector<ivl_scope_t> child;

      perm_string name_;
      perm_string tname_;
      perm_string file;
      perm_string def_file;
      unsigned lineno;
      unsigned def_lineno;
      ivl_scope_type_t type_;

      std::vector<ivl_type_t> classes;
      std::vector<ivl_enumtype_t> enumerations_;

      std::vector<ivl_signal_t> sigs_;

      unsigned nlog_;
      ivl_net_logic_t*log_;

      unsigned nevent_;
      ivl_event_t* event_;

      unsigned nlpm_;
      ivl_lpm_t* lpm_;

      std::vector<struct ivl_parameter_s> param;

	/* Scopes that are tasks/functions have a definition. */
      ivl_statement_t def;
      unsigned is_auto;
      ivl_variable_type_t func_type;
      bool func_signed;
      unsigned func_width;

      unsigned is_cell;

      // Ports of Module scope (just introspection data for VPI) - actual connections
      // are nets defined in u_.net (may be > 1 per module port)
      std::vector<PortInfo>     module_ports_info;

      unsigned ports;
      union {
	    ivl_signal_t*port;
	    ivl_nexus_t*nex;
	    NetNet**net;
      } u_;

      std::vector<ivl_switch_t>switches;

      signed int time_precision :8;
      signed int time_units :8;

      struct ivl_attribute_s*attr;
      unsigned nattr;
};
```



## tgt-vvp组件输出vvp文件

### tgt-vvp调用关系

​	在GitHub给出的官方文档当中，对于tgt-*/类似文件名的叙述是这样的：

>​	This core compiler, after it is finished with parsing and semantic analysis, uses loadable code generators to emit code for supported targets. The tgt-*/ directories contains the source for the target code generators that are bundled with Icarus Verilog. The tgt-vvp/ directory in particular contains the code generator for the vvp runtime.
>
>​	完成解析和语义后，此核心编译器分析，使用可加载的代码生成器发出支持的代码目标。 tgt-* /目录包含目标的源与Icarus Verilog捆绑在一起的代码生成器。 tgt-vvp /该目录特别包含vvp的代码生成器运行。











### 细节代码剖析

​	根据GitHub官网给出的官方文档可以知道，在iVerilog源码当中 tgt-vvp文件夹的作用是输出vvp文件，为此我们以 tgt-vvp 下Makefile的编译流程来为后续的代码剖析打下一个良好的基础。

- tgt-vvp 编译的时候需要用到的静态库

```Makefile
/tgt-vvp/Makefile.in

ifeq (@WIN32@,yes)
  TGTLDFLAGS=-L.. -livl
  TGTDEPLIBS=../libivl.a
else
  TGTLDFLAGS=
  TGTDEPLIBS=
endif
```

​	我们阅读 tgt-vvp文档下面的Makefile.in可以知道，在编译的时候会用到tgt-vvp的静态库进行编译。我们知道 ivl组件是整个 iVerilog编译器的核心部分，而tgt-vvp被称为可加载代码生成器部分因此两者关系密切相连。由于tgt-vvp组件的编译涉及到根目录下面的ivl.exe静态库的编译，为此我们深入分析根目录下静态库libivl.a的生成

```Makefile
M = LineInfo.o StringHeap.o

TT = t-dll.o t-dll-api.o t-dll-expr.o t-dll-proc.o t-dll-analog.o
FF = cprop.o exposenodes.o nodangle.o synth.o synth2.o syn-rules.o

O = main.o async.o design_dump.o discipline.o dup_expr.o elaborate.o \
    elab_expr.o elaborate_analog.o elab_lval.o elab_net.o \
    elab_scope.o elab_sig.o elab_sig_analog.o elab_type.o \
    emit.o eval.o eval_attrib.o \
    eval_tree.o expr_synth.o functor.o lexor.o lexor_keyword.o link_const.o \
    load_module.o netlist.o netmisc.o nettypes.o net_analog.o net_assign.o \
    net_design.o netclass.o netdarray.o \
    netenum.o netparray.o netqueue.o netscalar.o netstruct.o netvector.o \
    net_event.o net_expr.o net_func.o \
    net_func_eval.o net_link.o net_modulo.o \
    net_nex_input.o net_nex_output.o net_proc.o net_scope.o net_tran.o \
    net_udp.o pad_to_width.o parse.o parse_misc.o pform.o pform_analog.o \
    pform_disciplines.o pform_dump.o pform_package.o pform_pclass.o \
    pform_class_type.o pform_string_type.o pform_struct_type.o pform_types.o \
    symbol_search.o sync.o sys_funcs.o verinum.o verireal.o vpi_modules.o target.o \
    Attrib.o HName.o Module.o PClass.o PDelays.o PEvent.o PExpr.o PFunction.o \
    PGate.o PGenerate.o PModport.o PNamedItem.o PPackage.o PScope.o PSpec.o \
    PTask.o PUdp.o PWire.o Statement.o AStatement.o $M $(FF) $(TT)


ivl@EXEEXT@: $O $(srcdir)/ivl.def
	$(CXX) -o ivl@EXEEXT@ $O $(dllib) @EXTRALIBS@
	$(DLLTOOL) --dllname ivl@EXEEXT@ --def $(srcdir)/ivl.def \
		--output-lib libivl.a --output-exp ivl.exp
	$(CXX) $(LDFLAGS) -o ivl@EXEEXT@ ivl.exp $O $(dllib) @EXTRALIBS@

```

​	由根目录下面的Makefile.in文件摘取的这一段我们可以知道，libivl.a静态库的生成与ivl.exe生成的所含文件相同，只不过Makefile.in将其除了生成ivl.exe还生成了静态库libivl.a以供可加载代码生成器使用。



- tgt-vvp组件操作文件指针的代码逻辑

在tgt-vvp组件的目录下面，随处可见对于文件的操作函数例如 `int fprintf(FILE *stream, const char *format, ...)`函数。但是我们知道缓冲区类型有：全缓冲(大部分缓冲都是这类型)、行缓冲(例如stdio,stdout)、无缓冲(例如stderr)。关于全缓冲，例如普通的文件操作，进行fputs、fprintf操作后，数据并没有立即写入磁盘文件中，当fflush或fclose文件时，数据才真正写入。因此在tgt-vvp文件当中虽然很多源文件都有fprintf文件操作函数，但是并未直接写到文本文件上面而是大部分的时候将数据存入了缓冲区。查找整个tgt-vvp组件代码中出现`flcose()`和`fflush()`我们可以知道就在以下两个地方出现过，代码如下：

```c
/* draw_vpi.c */
		default:
		  fprintf(vvp_out, "\nXXXX Unexpected argument: call_string=<%s>, arg=%u, type=%d\n",
			  call_string, idx, ivl_expr_value(expr));
		  fflush(vvp_out);
		  assert(0);
	    }
	    args[idx].text = strdup(buffer);
      }

```



```c
/* vvp.c */
     
       /* Finish up any modpaths that are not yet emitted. */
      cleanup_modpath();

      rc = ivl_design_process(des, draw_process, 0);

        /* Dump the file name table. */
      size = ivl_file_table_size();
      fprintf(vvp_out, "# The file index is used to find the file name in "
                       "the following table.\n:file_names %u;\n", size);
      for (idx = 0; idx < size; idx++) {
	    fprintf(vvp_out, "    \"%s\";\n", ivl_file_table_item(idx));
      }

      fclose(vvp_out);
      EOC_cleanup_drivers();

      return rc + vvp_errors;		
```

​	我们可以知道整个tgt-vvp组件就这两个地方把缓冲区的数据写入到文件，这样子我们可以知道在iVerilog运行过程中，整体一次性将缓冲区数据存入文件效率要高于每一次都执行把缓冲区数据写入文件的操作，可以为我们后面的开发研究打下一个良好的基础。



- tgt-vvp 输出vvp文件大概过程

在tgt-vvp文件夹当中，是利用C语言当中的文件操作函数来进行文件操作达到输出vvp文件的目的。首先文件指针的定义在 tgt-vvp/vvp.c 当中定义，代码如下：

```C
/*
	filename: vvp.c
*/

FILE*vvp_out = 0;
int vvp_errors = 0;
unsigned show_file_line = 0;

int debug_draw = 0;
```

定义了一个文件指针，用来操作输出的vvp文件。对于文件指针的赋值操作 tgt-vvp 组件当中使用的是C语言当中的`fopen()`函数来进行赋值；代码如下：

```C
#ifdef HAVE_FOPEN64
      vvp_out = fopen64(path, "w");
#else
      vvp_out = fopen(path, "w");
#endif
```

vvp_out文件指针在此处赋值，对于path这个参数详细叙述在后面我们会进行。纵观整个tgt-vvp组件对于文件输出的操作函数就只有`fprintf`,`fputc`函数，可以知道虽然iVerilog代码量巨大但是对于输出vvp文件的思路还是常规的文件操作。



- tgt-vvp中 `fopen(path, "w")` 的 ’path‘入口参数详解

==TODO:叙述命令行---->driver----->fopen------>tgt-vvp==






- tgt-vvp如何使用根目录下面的 netlist form数据格式 

在ivl_target.h根目录下面的头文件中定义了众多API把信息传递给tgt-vvp组件产生vvp文件的原理如下：首先在tgt-vvp文件夹下面包含了如下的这些源文件：

<img src="directory.png" style="zoom: 50%;" />



​	在这些文件下每一个源文件都承担了输出vvp文件各自的任务，每一个文件的具体内容可以通过代码的具体注释得知。但是本说明文档的重点是了解gtg-vvp组件如何使用ivl_target.h当中的API来获取信息进行输出vvp文件。随便举一个代码例子如下：

```C
static void draw_lpm_repeat(ivl_lpm_t net)
{
      const char*dly = draw_lpm_output_delay(net, IVL_VT_LOGIC);

      fprintf(vvp_out, "L_%p%s .repeat %u, %u, %s;\n", net, dly,
	      ivl_lpm_width(net), ivl_lpm_size(net),
	      draw_net_input(ivl_lpm_data(net,0)));
}
```

​	该函数当中为tgt-vvp文件当中的一个功能函数，作用是向vvp文件输出某部分内容。客户要简单的看出函数当中使用了`fprintf`这个C语言当中的文本读写函数，向文件当中输出的信息利用的就是ivl_target.h当中的结构体指针。该函数的入口函数用ivl_lpm_t定义了一个结构体指针 `net`，指向引用了该结构体当中的信息。为此tgt-vvp就是利用ivl_target.h当中的API获取输出vvp文件需要的数据信息。

​	



​	



