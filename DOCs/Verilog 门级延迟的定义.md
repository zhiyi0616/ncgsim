# 门级延迟定义

Verilog 使用 Specify 和 # 定义延迟。SDF文件又单独定义了门级的延迟。


# Verilog 门级延迟
- 分布式延迟
  - 基于单个元件定义，assign 的延迟。
- 集总延迟
  - 模块延迟，整个模块的延迟。
- 路径延迟
  - 引脚到引脚的延迟。就 Verilog 而言，一般定义在模块内部。


## Specify 块
Verilog 中使用 Specify 块定义路径延迟。
```verilog
specify
    (A=>B)=5 // Case1: 对应位连接，AB必须等宽
    (A*>B)=5 // Case2: 全连接，笛卡尔积
endspecify
```

- iVerilog 对 Specify 的支持
  - iVerilog 会直接无视 specify 块。
- ModelSim 对 Specify 的支持
  - Modelsim 不会忽略 Specify 块。





