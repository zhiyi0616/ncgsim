# VVP Parser
此程序已实现将.vvp文件中的语句进行词法分析，其中，`.vvp` 语句各成分介绍来自于 iVerilog 源码中vvp文件夹的 `readme.txt`。

## 通用格式：


一个vvp程序文件是很多语句的集合。其中一般情况下，每个语句由一个*label*、一个*opcode*以及依赖于该*opcode*的一组或几组*operand*。

每条语句以分号结束，分号后的字符串即为注释。

根据对案例的调研，VVP文件结构一般如下：

   + 头定义
   + 声明语句
   + 操作码语句
   + 文件信息

因此本次任务最终目的是将每一句vvp语句剥离开来，并且输出其对应的类型（label、opcode、operand或注释等）


-------------

## 各成分特征及介绍

+ **1. 头定义**

VVP的头定义要**在任何一条非注释语句之前，并且以冒号(:)开头。** 它们用于将参数或全局变量从编译器传递到vvp的执行阶段。一般格式为：
```
:module "name" ;
```
例句：
```
:ivl_delay_selection "TYPICAL";
```
+ **2. 分号**

每个语句都必须**以分号结尾**，也可以在分号后**添加注释**。

但无论是否有注释，分号必须作为一个**语句结束的标志**。

+ **3. labels**

根据/vvp/Readme.txt/中的介绍，vvp文件中，*label*和*symbol*可以由以下几个部分组成：
```
a~z  (即小写英文字母)
A~Z  (即大写英文字母)
0~9  (即数字0到9)
.$_<>(小数点、美元符号、下划线以及尖括号)
```
其中，label 不允许以数字和小数点开头，这一点与大多数编程语言的变量名取名方式一致。

**注：** 在对案例的观察中我们发现，label 不止是由以上所提及的几种符号组成，在*label*中**有存在“/”的情况**，因此，在我们的程序中，弥补了这一定义上的缺漏。

例如：
```
E_0000000004607e90/0 .event negedge, v00000000045e2cc0_0;
```
其中 `E_0000000004607e90/0` 为 label，但其含有 `/` 符号。


+ **4. opcode**

opcode 一般是以 “ . ”（小数点） 开头或以 “ % ” （百分号）开头，并以空格结束。

+ **5. operand**

对于 operand ，/vvp/readme.txt/中的介绍不多，我们将 opcode 后一直到分号前的语句都作为 operand ，他们以逗号分隔。

**注：** 在对vvp文件的调研中我们发现，构成 operand 的字符有很多种，但他们都以逗号分隔，以目前的案例结果来看，operand 可以以**双引号(")、英文字母大小写、数字、特殊符号**等开头。目前编写程序时只能不断查漏不断添置。

---------------

## 程序介绍

**基本思路以及执行过程**：

本次程序使用Python语言编写，利用了Python中面向对象的程序设计的思想。

1. **读入文本**
2. **语法分析**
3. **分析冗余信息**
4. **报错**
5. **交互功能**
6. **JSON输出**
7. **辅助功能**





+ **2、Tokens定义代码段**

由于vvp语句的各成分有上述特征，因此，我们在整个程序的开头对各类tokens进行了定义。

部分代码段如下：
```
        DIGITS  =  '0123456789'
        ALPHABET  =  'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
        SPECIAL_SYMBOLS  =  '.$_/<>"'
   ```
其中，*DIGITS*为数字定义，*ALPTHABET*为英文字母大小写的定义，*CHARS*为字符定义。

+ **3、Token类**

该代码段的作用即为接收到各 `make_func()` 函数所传递过来的token参数，并**将其格式化输出**。

+ **4、Parser 类**

Parser 类完成的功能是读取输入进来的'text'并进行词法分析。

简易流程图如下：

![Parser 类](doc_imgs/code_flow.png)

**4.1、make_tokens()函数** 

*make_tokens()* 函数是整个 Parser 类的核心。

此函数用一系列 *if-elif* 语句，对读取的语句进行判断分析：如若遇到空格、制表符以及逗号程序将直接跳过。因为除双引号内的空格或制表符以外，其他的空格均为分隔符的作用，可直接跳过。

**需要特别注意的是：**

由于*operand*的命名规则当中基本包含了*label*的所有命名规则，但*operand*必然存在于*label*之后，因此，在判断当前字符是*label*还是*operand*时，若不加以一个状态变量(isopcode)来判别当前字符位置是否已遍历*opcode*，将会出现混乱。

存在于*make_opcode*函数后，当程序已经遍历过整个*opcode*时，将其置为1，因为在*opcode*之后的只能是*operand*。这样便有效地避免了*label*和*operand*出现混乱的情况。

具体实现代码段如下：
```python
            def make_opcode(self):
               opcode_str  =  ''
               while self.current_char ! =  ' ':
                     opcode_str + =  self.current_char
                     self.advance()
               return Token(TT_OPCODE, opcode_str)

             .........

        elif self.current_char in ALPHABET + '$_<>' + '"' + DIGITS:
                if isopcode  =  =  0:
                    tokens.append(self.make_label())
                elif self.current_char ! =  None:
                    tokens.append(self.make_operand())
            ...........
```

**注：本段已做大部分修改，具体见下文。**


其他代码雷同。

**4.2、make_func()函数**

make_func()函数指的是将各个成分内容存储下来并将其返回的函数。

有 ***make_headfile()、make_label()、make_opcode()、make_operand()以及make_comt()等***

编写思路很简单，先建立一个空数组，接着进入*while*循环，*while*循环的条件就是该语句类型的限定条件。

如*make_label()*中，根据vvp语法，*label*和*opcode*之间的分隔符为空格，所以，在判断*label*的时候，只需要判断当前字符是否为“ ”（空格）即可。

代码段如下：

    def make_label(self):
            label_str  =  ''
            while self.current_char in ALPHABET + CHARS + DIGITS:
                label_str + =  self.current_char
                self.advance()
            return Token(TT_label, label_str)

**值得注意的是：**

由于*operand*中的分隔格式不一致，有逗号也有空格。所以我们做如下处理：

        ......
        while self.current_char not in ',' + ';':
            operand_str + =  self.current_char
            self.advance()
        .....

即：**若当前字符不是逗号或分号时，执行循环语句。**

+ **5、Error类**

报错类的思路就是如果当前字符不满足以上所有的规则，那么将返回当前这个字符，并输出“*Illegal charactor*”

但由于技术还不是很成熟，所以报错类的程序还没有做得非常完善，仅仅能解决一些普通的非法字符。这个将在以后的程序中完善。

+ **6、shell.py程序文件**

*shell*文件就是起到一个调用整个*vvp*文件并且将最终返回的结果“*result*”以及“*error*”打印显示。

----------------

## 第二次代码完善以及修复内容解释

+ **1、修改常量名**
+ **2、添加两个状态变量**
+ **3、解决跳过引号内空格、逗号或分号的问题**
+ **4、读取vvp文件**
+ **5、输出json格式**


-------------------------


### 具体分析：
+ **1、修改常量名**
  
为了使得代码**可读性更强**并且**提高代码的灵活性**，本次对常量的名称进行了如下修改：

```
IN_LABEL_SYMBOLS  =  DIGITS + ALPHABET + '$_/<>'
START_OPCODE_SYMBOLS  =  '.' + '%'
IN_OPERAND_SYMBOLS  =  DIGITS + ALPHABET + '$_/<>"+-*'
START_HEADDEF_SYMBOLS  =  ':'
```
其中，`IN_LABEL_SYMBOLS`代表的是 **label 中所含有的符号**， `START_OPCODE_SYMBOLS`表示为 **opcode 的起始符号**。其他两个雷同。

+ **2、添加两个状态变量**

在之前的代码中，我们使用的是全局变量 `isopcode` 作为判断是否遍历完 opcode 的标志。但是在之后的实践中我们发现，如若 opcode 的个数大于1，则会出现错误的输出。

因此，作出如下添加：
```
self.after_opcode  =  False       # True if opcode is passed
self.after_semicolon  =  False    # True if the statement is finished
```
在遍历完 opcode 后，将`after_opcode`置为 *True* 。

`after_semicolon`为语句结束的标志变量。

+ **3、解决跳过引号内空格、逗号或分号的问题**
  
在 operand 中，常会出现引号，而引号内又可能出现空格、逗号或分号，但这些符号同时是作为 label、opcode 以及 operand 之间的分隔标志。因而会造成在分析 operand 时跳过上述符号的情况。本次修改中，已将此问题妥善解决。

代码段如图所示：
![get_quote_str](doc_imgs/get_quote_str.png)

**几点解释**：

1. `assert` 函数可以在条件不满足程序运行的情况下直接返回错误，而不必等待程序运行后出现崩溃的情况。该语句作用为，确保当前已经进入引号内，并且从非引号字符开始读入
2. `while` 的循环条件，由于 `start_char` 是双引号，所以，当程序未遇到另一个双引号前一直循环，这样可以确保引号内的所有字符都被读取并存储进 `res_str` 列表中。
   
+ **4、读取vvp文件**

之前的代码中，我们实现了在编译器终端输入语句并进行分析。本次在*shell.py*中添加了对vvp整个文件进行读取的功能，从而可以直观地看到所有语句的分析情况。

采用了 `argparse.ArgumentParser()` 以及`readline()`函数。

其中，`argparse.ArgumentParser()`主要有以下三个步骤：

1. 创建 ArgumentParser() 对象
2. 调用 add_argument() 方法添加参数
3. 使用 parse_args() 解析添加的参数
   
具体代码段如下：
```
.......
arg_parser  =  argparse.ArgumentParser()
arg_parser.add_argument("file",help = "File name",default = None,nargs = "?",type = argparse.FileType('r'))
args  =  arg_parser.parse_args()
.......

for row in file.readlines():
        line_num + = 1
        result, error  =  vvp.run(row)
.......

```

+ **5、输出json格式**
  
思路如下：

1. 逐行读入vvp文件
2. 进行词法语法分析
3. 使用 `___dict__` 函数将输出的结果转化成“键-值”模式
4. 新建一个列表存储以上结果
5. 打开一个文件并写入
6. 使用 `json.dump()` 函数将上述列表中的结构转化为 json 格式并将结果存入文件中

最终实现以下结果：

![json](doc_imgs/json_img.png)

## 第三次代码完善及修复内容解释

本次修改完成了对 vvp 文件进行更进一步的分析，将文件中的逻辑门、所处scope 块、var 型变量以及 net 型变量输出为json。具体如下：
+ **1. 输出对应 scope 块**
+ **2. 提取 vvp 文件中的逻辑门信息**
+ **3. 提取 var 变量信息**
+ **4. 提取 net 变量信息**
+ **5. 修改输出格式，将列表转为字典**

----------

+ **1.输出对应 scope 块**

根据官方文档中的解释， scope 语句中位于句首的将是它的 label ，并且只有根 scope 才能使用简短形式 (详见 `vvp.md` 文档说明) ，所以判别该语句的 opcode 是 “.scope” 时，将句首的 label 返回，便是代表了该 scope 块。

具体代码如下：
```python
def find_scope(tokens = [],last_scope = None):
    is_scope = 0
    if len(tokens)>2:
        if tokens[1].type =  = "OPCODE" and tokens[1].string =  = ".scope":
            is_scope = 1
    if is_scope =  = 1:
        return tokens[0].string
    else:
        return last_scope
```

+ **2. 提取 vvp 文件中的逻辑门信息**

由于最终要分析 vvp 文件所蕴含的内容以及电路，因而必须将文件中的逻辑门信息输出。

基本思路是：

首先通过上述进程找到当前 scope ，然后对读取的每一句语句的 tokens 进行判别，如若其 opcode 的 type 是 `.functor` ，那么将 `.functor` 前的 label 放入“label” 中，之后的 operand 读取进 “functor” 以及 “input_width” 中。再将第一个逗号后一直到分号为止的以逗号分隔的各个 operand 都作为输入存入 “inputs” 列表中。

具体代码如下：
```python
 def make_logic_cell(self,tokens = [],last_scope = None):
        is_logic_cell = 0                        #Determine if there is a logic gate，its value is 0 or 1.
        current_scope = find_scope(tokens,last_scope)
        for i in tokens:
            if i.type =="OPCODE" and i.string == ".functor":
                is_logic_cell = 1
        if is_logic_cell == 1:
            count = -1
            for i in tokens:
                count+ = 1
                if i.type == "LABEL" and i.string! = None:
                    self.label = i.string
                    
                elif i.type == "OPCODE" and i.string =  = ".functor":
                    count+= 1
                    split_blank = tokens[count].string.split(" ")   #OPECODE will be divided into functor and inputs_width
                    self.functor = split_blank[0]
                    self.inputs_width = split_blank[1]
                else:
                    if(count == 3):
                        continue
                    else:
                        if(i.type! = "CMT"):
                            self.inputs.append(i.string)
            self.scope = current_scope
        return is_logic_cell ,current_scope
```

## vvp文件冲突列表

在本次开发中，出现了很多与 **/vvp/README.txt** 中冲突的部分，无形中增加了开发困难。

汇总如下表：


| 序号  |                 冲突内容                 | iVerilog官方README中的叙述 |
| :---: | :--------------------------------------: | :------------------------: |
|   1   |         "#"号后语句未被明确定义          |  未说明#号开头的注释形式   |
|   2   |  实际VVP文件中组成 Label 的符号中有“/”   |        不能出现“/”         |
|   3   |       实际VVP文件中出现带符号数字        |       数字必须无符号       |
|   4   |     opcode 的起始标志出现“%”以及“.”      |         无明确定义         |
|   5   |    实际VVP文件operand 中的空格及逗号     |  逗号分隔参数，空格未定义  |
|   6   |  带引号的operand中出现空格、逗号或分号   |         无相关叙述         |
|   7   |      文件末尾filenames的不明确定义       |        与头定义混淆        |
|   8   |         scope语句中的`<parent>`          |         未解释说明         |
|   9   |              .timescale语句              |         无相关叙述         |
|  10   |                .port_info                |         无相关叙述         |
|  11   | %pushi/vec4 语句真值表与实际测试结果不符 |    详见 `vvp.md` “十二”    |