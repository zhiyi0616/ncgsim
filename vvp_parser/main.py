from vvp import Parser
#from gate import LogicCell, Var, Net, Thread , statement_classify
import analyse
import json
import argparse

arg_parser = argparse.ArgumentParser()
arg_parser.add_argument(
    "-v", "--verbose", help="Verbose output", action='store_true')
arg_parser.add_argument("-o", "--output-file",
                        help="Output JSON file", default='netlist.json')
arg_parser.add_argument(
    "--dff", help="Define D flip-flop module name", type=str)
arg_parser.add_argument("file", help="Input vvp file name",
                        default=None, nargs="?", type=argparse.FileType('r'))
args = arg_parser.parse_args()

if args.file:
    # OUTS
    out_all = {}                  # The outer json container of all sections.
    statement_list = []         # To store all statements(parsed).

    thread_dict = {}              # To store the thread finding result.

    # TEMP
    thread_proc_list = []         # To temporarily store thread procs

    current_scope = None          # To indicate the scope in which the cell lays
    have_err = False
    line_num = 0
    prev_row = ""                 # For multi-line statements

    parser = Parser()
    # Read and parse
    for row in args.file.readlines():
        
        line_num += 1
        row = row.strip('\n')
        row = prev_row+row
        if line_num<=480 and line_num>=458:
            print(row)
        # Here a logic-row is read in
        result, error, finish_flag = parser.run(row)
        # The currrent line(with prev) won't finish current statement
        if finish_flag == False:
            prev_row = row
            continue                        # Skip and read the next line
        prev_row = ""

        # Temporarily store the statement for thread finding.
        thread_proc_list.append(result)
        if error:
            have_err = True
            print(error.as_string(), "line", line_num)
        elif result:
            # statement_list.append([token.__dict__ for  token in result])
            statement_list.append(result)

        if args.verbose:
            if not line_num %1:         # Print prompts if the file is large.
                print("%d rows parsed." % line_num)
    if have_err:
        print("Err occured.")
        exit()

    if args.verbose:
        print("Creating detailed statement information...")
    logic_cell_dict, net_dict, scope_dict, var_dict, port_list = \
        analyse.fill_buckets(args.verbose, statement_list)

    if args.verbose:
        print("Attaching signal names to scopes...")
    analyse.attach_signal_names(scope_dict, var_dict, net_dict)

    if args.verbose:
        print("Adding cell relationship...")
    analyse.add_logic_relation(
        args.verbose, logic_cell_dict, var_dict, net_dict
    )

    if args.verbose:
        print("Adding scope relationship...")
    analyse.add_scope_relation(args.verbose, port_list, scope_dict)
    # # Classify and put to bucket
    # for statement in statement_list:
    #     opcode = statement.get("opcode")
    #     if opcode and opcode==".functor":
    #         label = statement["label"]
    #         logic_cell_dict[label] = statement

    #    # Classify the statements
    #    statement_class,tokens=statement_classify(result)   # result is the statement tokens
    #    if statement_class=="scope":
    #        current_scope=tokens[0].string  # Update current_scope
    #        scope_dict[current_scope] = {   # fixed: Here we take the tokens[2] is the type, [3] as name.
    #            "label":current_scope,
    #            "type": tokens[2].string,
    #            "instance_name": tokens[3].string.split()[0], # fixed: The class name of the module statement in verilog.
    #            "definition_name": tokens[3].string.split()[1],   # The name of the definition.
    #            "is_top":True if len(tokens)==4 else False,       # if the scope statment is the short form, that represents a top module,and this flag is True
    #            "is_dff":True if args.dff == tokens[3].string.split()[1] else None,  # determine if the scope is a dff.
    #            "input_ports":{},
    #            "output_ports":{},
    #            "bidi_ports":{}
    #        }
    #    if statement_class=="logic_cell":
    #        logic_temp=LogicCell(tokens,current_scope)
    #        logic_cell_dict[logic_temp.label]=logic_temp.__dict__
    #
    #    elif statement_class=="var":
    #        var_temp=Var(tokens,current_scope)
    #        var_dict[var_temp.label]=var_temp.__dict__
    #
    #    elif statement_class=="net":
    #        net_temp=Net(tokens,current_scope)
    #        net_dict[net_temp.label]=net_temp.__dict__
    #
    #    elif statement_class=="thread":
    #        thread_temp=Thread(tokens[1].string,thread_proc_list)
    #        thread_dict[thread_temp.label]=thread_temp.__dict__
    #        thread_proc_list=[]         # Clear the cache when a .thread is found

    # with open("tmp_statementsjson","w+") as f:
    #     json.dump(statement_list,f,indent=4)
    #
    # # Find logic cell relationships
    # if args.verbose:
    #     print("Finding logic cell relationships...")
    # for cell_label ,cell in logic_cell_dict.items():
    #     for input in cell["inputs"]:                # In a cell, the input could be cell, net, var, or constant
    #         if logic_cell_dict.get(input):          # The input of current cell is driven by a logic cell;
    #             if logic_cell_dict[input].get("drives"):
    #                 logic_cell_dict[input]["drives"].append(cell_label)
    #             else:
    #                 logic_cell_dict[input]["drives"] = [cell_label]
    #         elif var_dict.get(input):               # The input of current cell is driven by a var;
    #             if var_dict[input].get("drives"):
    #                 var_dict[input]["drives"].append(cell_label)
    #             else:
    #                 var_dict[input]["drives"] = [cell_label]
    #         elif net_dict.get(input):               # The input of current cell is driven by a net;
    #             if net_dict[input].get("drives"):
    #                 net_dict[input]["drives"].append(cell_label)
    #             else:
    #                 net_dict[input]["drives"] = [cell_label]
    #         else:
    #             if args.verbose:
    #                 print("Unknown type of input:",input)
    if args.verbose:
        print("Dumping JSON...")
    # Dump
    out_all["statements"] = statement_list
    out_all["logic_cells"] = logic_cell_dict
    out_all["vars"] = var_dict
    out_all["nets"] = net_dict
    out_all["threads"] = thread_dict
    out_all["scopes"] = scope_dict

    with open(args.output_file, "w+") as f:
        json.dump(out_all, f, indent=4)

else:       # Interactive mode
    prev_row = ""
    finish_flag = True
    parser = Parser()
    print("You are now in interactive mode. Type '$' to quit.")
    while True:
        input_prompt = "vvp_parser > " if finish_flag else "       ... > "
        row = prev_row + input(input_prompt)
        if row and row[0] == "$":
            finish_flag = True
            exit()
        else:
            result, error, finish_flag = parser.run(row)
        if not finish_flag:
            prev_row = row
            continue                        # Skip and read the next line
        prev_row = ""
        if error:
            print(error.as_string())
        else:
            print(result)
