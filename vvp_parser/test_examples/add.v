`timescale 1ns/100ps
module add(f0,f1,a,b,c,d);

input a;
input b;
input c;
input d;
output reg f0,f1;

always @(posedge d)
begin
    if(!b)
    f0 <= a&c ;
    else
    f0 <= a|c;
end

endmodule