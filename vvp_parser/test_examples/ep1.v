`timescale 1ns/100ps
module ep1(f0,f1,a,b,c,clk);

input a;
input b;
input c;
input clk;
output  reg f0,f1;

always@(posedge clk)
begin
    #5;
    f0 <= #7 a&b;
    f1 =  a|c;
end

endmodule