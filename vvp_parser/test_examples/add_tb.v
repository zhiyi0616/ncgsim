`timescale 1ns/100ps
module add_tb();
wire f0_tb;
wire f1_tb;
reg a_tb;
reg b_tb;
reg c_tb;
reg d_tb;

initial begin
    $dumpfile("add_tb.vcd");
    $dumpvars(0,add_tb);
    a_tb = 0;
    b_tb = 0;
    c_tb = 0;
    d_tb = 0;
    #15 a_tb = 1;
    #15 c_tb = 1;
    #20 b_tb = 1;
    #20 a_tb = 0;
    #25 b_tb = 0;
    #20 c_tb = 0;
    #25 c_tb = 1;    
    #100 ;
end

add DUT(f0_tb,f1_tb,a_tb,b_tb, c_tb,d_tb);

endmodule