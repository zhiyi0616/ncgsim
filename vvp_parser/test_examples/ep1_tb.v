`timescale 1ns/100ps
module ep1_tb();
wire f0_tb;
wire f1_tb;
reg a_tb;
reg b_tb;
reg c_tb;
reg clk_tb;

initial begin
    clk_tb = 0;
    forever #100 clk_tb = ~clk_tb; 
end

initial begin
    $dumpfile("ep1_tb.vcd");
    $dumpvars(0,ep1_tb);
    a_tb = 0;
    b_tb = 0;
    c_tb = 0;

    #15 a_tb = 1;
    #15 c_tb = 1;
    #20 b_tb = 1;
    #20 a_tb = 0;
    #25 b_tb = 0;
    #20 c_tb = 0;
    #25 c_tb = 1;    
    #100 ;
    #2000 $finish;
end

ep1 DUT(f0_tb,f1_tb,a_tb,b_tb, c_tb,clk_tb);

endmodule