`timescale 1ns/100ps
module ep2(f2,f1,f0,a,b,c,d);

input a;
input b;
input c;
input d;
output f0,f1,f2;

and  a1(f0,a,0,b);
assign f1 = a | b;
assign f2 = c ^ d;

endmodule