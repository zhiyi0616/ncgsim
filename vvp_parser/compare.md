# Verilog 文件与 VVP 文件的对比

本文档主要记录一些简单的 Verilog 程序及其与 vvp 文件的对应关系，以进一步阐述 Verilog HDL 语句与 vvp 语句的转化。先从完成简单的功能开始。

## 一、组合逻辑(与门)
这是一个及其简单的功能，在 verilog 的 RTL 级语言中，仅需要利用运算符 "&" 对输入进行运算，并且用 `assign` 对表达式进行赋值。具体程序如下：
```verilog
module add(f0,a,b);

input a;
input b;

output f0;

assign f0 = a & b ; 

endmodule
```

编写 test_bench文件如下：
```verilog
`timescale 1ps/1ps
module add_tb();
wire f0_tb;
reg a_tb;
reg b_tb;

initial begin
    $dumpfile("add_tb.vcd");
    $dumpvars(0,add_tb);
    a_tb = 0;
    b_tb = 0;
    #10 a_tb = 1;
    #2 b_tb = 1;
    #100 ;
end

add DUT(f0_tb,a_tb,b_tb);

endmodule
```
使用 gtkwave 仿真图如下：
![与门](../doc_imgs/and.png)
由仿真图可知，程序运行无误。经过 iVerilog 生成的vvp文件如下：

![and.vvp](../doc_imgs/and_tb.vvp.png)

下面进行对比分析。

在vvp文件中，首行 `#!` 开头的语句为**执行器路径**。紧接着冒号开头分号结尾的语句为**头语句**，头语句中包含了版本号、延时模式选择、时间精度以及模块信息等内容。从第十行开始，便进入了 `scope` 语句，在出现下一个 `scope` 之前都是属于同一个 `scope` 语句块。

1. 第十行是 `scope` 语句的简短形式，说明此 `scope` 块为“根 scope ”。该行指代的信息是：**在此处声明了一个 `scope` ，其类型是 `module` ，实例名为 “add_tb”  ，定义名为 “add_tb” ，在源文件中定义的行号为2。
2. 第十一行，声明了这个模块中的时延单位和时延精度都为10的负十二次方，即 verilog 文件中所定义的 `1ps/1ps`。
3. 第十二至十四行对应 verilog 文件的3~5行，是变量的声明。**reg 型变量将用 variabel 语句声明，wire 型变量将与 net 语句声明。** 再次强调，对于 net 语句，其句尾的 `<symbol>` 部分才是该变量的指代名，句首的 `<label>` 仅仅出现一次。
4. 第十五行开始，进入下一个 `scope` 块。这个 `scope` 块是前一个 `scope` 的子模块。对应 test_bench 文件中 的18行，映射了 `add.v` 文件的所有信息。
5. 第十六行与十一行含义相同。
6. 第十七行至十九行是对端口信息的声明。
7. 第二十行，完成“与”门功能，利用 functor 语句，类型为 `and` ，其 `label` 定位至输出 `f0_tb` ，位数为1位。根据 `symbol_list` 对应关系可看出，输入是 `a_tb` 、 `b_tb` 以及两个“C4<1>”，因为输入不满4个，且完成的是“与”门功能，所以将剩余两个输入置为1，这样不会影响最终输出。
8. 第二十一行至二十三行为源文件的变量声明。
9. 第二十四行的形式在官方文档中并未提及过。
10. 第二十五行，`T_0` ，官方文档同样没有相关叙述，但是我们猜测，这是一个线程的编号。在 `T_0` 这个线程中基本都对应着原 `add_tb.v` 文件中的语句。
11. **注**：vvp 语句中是通过 **%pushi/vec4 - %store/vec4** 组合进行连续赋值操作的。对 `%pushi/vec4` 语法的更正在 `vvp.md` 中已有阐述，这里不再进行说明。
12. 到第四十行之前都是进行动作存储，当读取到第四十行时，便开始线程 `T_0` 执行前面所有的语句。
13. 最后是文件列表，官方文档中还未提及。

## 二、简单时序逻辑电路

观察上一个例子中，加入时序逻辑时的vvp文件变化。在源文件中增加的语句块很简单：
```verilog
always@(posedge clk)
begin
    f0 <= a & b ;    
end
```
生成的新的 vvp 文件中，新的线程块如下：
```
 1   %wait E_00000000011a7600;
 2   %load/vec4 v0000000000842a10_0;
 3   %load/vec4 v0000000000842ab0_0;
 4   %and;
 5   %store/vec4 v00000000011aed20_0, 0, 1;
 6   %jmp T_0;
 7   .thread T_0;
```
1. 第一行意为等待该 `label` 所指代的事件—— `clk` 上升沿的到来。
2. 第二三行是将即将要进行操作的输入加载如 vec4 堆栈。
3. 第四行是对刚刚加载进入 vec4 堆栈的变量进行“与”操作。
4. 第五行是将上一行的结果存入 vec4 堆栈并输送给 `f0` 。
5. 第六行代表再次跳转到线程 `T_0` 的开头，也即完成了一次“与”操作后继续等待 `clk` 的上升沿。

## 三、门级描述与 RTL 级描述在 vvp 文件中的区别

门级描述即使用门的关键字(如：`xor`、`and` 以及 `or` 等)来描述一个门的输入和输出，例句：
```verilog
and y(f, a, b);
```
以上语句便是定义了一个名为 `y` , 输入为 `a` , `b`, 输出为 `f` 的一个“与”门。在 vvp 文件中使用 `functor` 语句表示：
```
L_0000000004545120/d .functor and 1, v000000000465abe0_0, v000000000465ac80_0, C4<0>, C4<0>;
```
若使用 RTL 级语言描述该功能，则是如“ 一、‘与’门 (组合逻辑)”中所述。

生成 vvp 文件后，我们发现，**在描述同一种门的功能时，使用门级语言描述和 RTL 级语言描述所生成对应相关功能的 vvp 语句是一致的。**

使用其他门测试后，以上结论依然成立。

## 四、verilog 中的组合逻辑门及其门级描述在 vvp 文件中的体现

**逻辑门**:
+ and (output,input,…)
+ nand (output,input,…)
+ or (output,input,…)
+ nor (output,input,…)
+ xor (output,input,…)  
+ xnor (output,input,…)

**缓冲器和与非门**：
+ buf (output,…,input)
+ not (output,…,input)

**三态门**：
+ bufif0 (output,input,enable)
+ bufif1 (output,input,enable)
+ notif0 (output,input,enable)
+ notif1 (output,input,enable)

**经测试，当使用这些门级描述语言时，其对应的 vvp 文件中的语句，均为 `functor` 语句。除此之外，`functor` 语句出现时，均表示逻辑门的功能。**

## 附：文档内未提及的发现

### 1、关于 label 名称

在官方文档的叙述中，label 是一组由字母、数字、下划线以及特殊符号组成的**随机数**。但是在实际测试中，不难发现，label 的命名方式有以下几条规律：

+ 变量名(包括 `var` 型和 `net` 型)的 label 通常以 **“v”** 开头。
+ 逻辑门(存在于 functor 语句中)的 label 通常以 **“L_”** 开头。
+ 事件语句(存在于 event 语句中)的 label 通常以 **“E_”** 开头。
+ `scope` 语句的 label 通常以 **“S_”** 开头。
  
### 2、var 变量和 net 变量的 label 名有迹可循

经过观察比对发现，**连续定义**的 var 型变量以及 net 型变量的 label 之间**具有关联性**。

每一个 var 变量和它后面紧接着定义的同类型变量之间的 label 名**采用16进制**编写，并两者之间相差一个**十进制的9或者10**。是因为他所代表的是该变量在内存中所占用的地址。每一个连续的变量，占用的内存地址也是连续的。

### 3、net 变量的驱动

vvp 文件中，在每一个 net语句的注释里都会出现如 `1 drivers` 内容。

字面理解，就是指该变量是否作为**受到某一功能的驱动**，若是，受到几个驱动。有如下语句：
```
v00000000001fb810_0 .net "a", 0 0, o0000000001053cd8;  0 drivers
v00000000001fbdb0_0 .net "b", 0 0, o0000000001053d08;  0 drivers
v00000000001fbc70_0 .net "c", 0 0, o0000000001053d38;  0 drivers
v00000000001fb090_0 .net "d", 0 0, o0000000001053d68;  0 drivers
v00000000001fb9f0_0 .net "e", 0 0, L_00000000010329d0;  1 drivers
v00000000001fbf90_0 .net "f", 0 0, L_0000000001032a40;  1 drivers
v00000000001fb770_0 .net "out", 0 0, L_0000000001032ab0;  1 drivers
```
上述 vvp 语句出自 `xor.v` 文件，原语句为：
```verilog
  xor #7 a1(e,a,b);
  xor #3 a2(f,c,d);
  xor a3(out,e,f);
```
根据对比，可以看出，作为输入的 a b c d 四个变量都是 `0 drivers` ,而作为输出的 e f out 都是 `1 drivers` 。在此例子中， e f 和 out 分别受到异或门 a1、a2 和 a3 的驱动，因此他们是 `1 drivers`。而对于 a b c d 四个输入，他们没有受到其他功能或引脚的驱动，因此他们是 `0 drivers` 。

对于以下例子：
```
v00000000010629a0_0 .net "a", 0 0, v00000000001acb80_0;  1 drivers
v0000000001062a40_0 .net "b", 0 0, v00000000001acc20_0;  1 drivers
v0000000001062ae0_0 .net "c", 0 0, v00000000001accc0_0;  1 drivers
v00000000001ac9a0_0 .net "d", 0 0, v00000000001acd60_0;  1 drivers
v00000000001aca40_0 .net "f0", 0 0, L_0000000001092d50;  alias, 1 drivers
v00000000001acae0_0 .net "f1", 0 0, L_0000000001092dc0;  alias, 1 drivers
```
原 verilog 语句为：
```verilog
and a1(f0,a,b);
or a2(f1,d,c);
```
在这个例子中， a b c d 作为输入，注释中均注明是 `1 drivers` ，是由于这四个变量，分别受到了 test bench 文件中的 a_tb b_tb c_tb 和 d_tb 的驱动。而作为输出的 f0 f1，则是受到两个门电路的驱动，所以也是 `1 drivers` 。


### 4、关于线程 `T_label`

在 `vvp.md` 中已有叙述，只有 initial 块、always 块或者其他可能开启一个线程的事件。但是在每个线程中，可能会有如“if-else”的判断语句，从而会从一个线程跳转至另一个线程，通过 `%jmp` 语句实现 。每一个线程以 `T_label` 形式开头，一直到 `.thread T_label` 语句截止。无论中间出现多少子线程，我们都把他认为是最头部线程的内容。