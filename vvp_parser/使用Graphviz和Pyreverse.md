# 使用Graphviz和Pyreverse绘制Python项目结构图
## 简介
- `Graphviz` 开源的图形绘制工具包
- `Pyreverse` 分析 Python 代码和类关系的工具
## 安装
1. 安装 `Graphviz` [官网下载地址](https://www2.graphviz.org/Packages/stable/windows/)(若链接失效，可直接搜索 Graphviz 进入官网 Download 下载)
   
   注意添加 `bin/` 目录的路径到系统路径，测试是否安装成功：
   `dot --help`
   
2. 安装 `Pyreverse` 
   现在 `Pyreverse` 已经集成到 `pylint` ，直接安装 `pylint` 即可
   `pip install pylint`
   测试是否安装成功
   `pyreverse --help`

3. 使用 `Pyreverse` 分析 Python 代码

   以 `main.py` 代码为例
   `pyreverse main.py`

   Pyreverse 会分析 `main.py` 文件的代码并在当前目录下生成 `classes.dot` 和 `packages.dot` 两个 `dot` 格式的文件

4. 使用 `Graphviz` 将 `dot` 文件转换为图形格式
   
   转换为 `png` 格式：
`dot -Tpng -o classes.png classes.dot `
   
   `dot -Tpng -o packages.png packages.dot`
   

也可以转换为 `jpg` 和 `pdf` 格式
   `dot -Tjpg -o classes.jpg classes.dot `

   `dot -Tpdf -o packages.pdf packages.dot`

## 本次遇到的问题及解决方案

### pyreverse 安装失败

**情况：** 提示没有找到 pyreverse 安装包或者没有指令。

**原因：** pyreverse 下载路径错误。

**解决办法：** 确保把 python 的 Scrips 路径写入环境变量中，使用指令

`pip install --target=E:\python\Lib\site-packages`

将 pyreverse 安装至 python 根目录中的安装包路径中。安装成功后，\python\Lib\site-packages\pylint路径下会增加一个“pyreverse”文件夹。

再在 cmd 中使用指令 `pyreverse` 发现已经安装成功。



### Graphviz 安装失败

**情况：** 使用官方源码编译失败。

**原因：** 尚不明确，可能是 MSYS2 缺少必要的包。

**解决办法：** 安装 Windows 版本的 exe 文件。一步一步安装好后，将 graphviz 文件夹下的 \bin 路径写入环境变量中，然后即可使用。

