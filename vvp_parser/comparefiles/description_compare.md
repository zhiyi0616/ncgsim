# 结构描述与过程描述对比

本文档给出一系列 verilog 文件与 vvp 文件的对比关系。与 `compare.md` 文件不同的是，本文档旨在记录在同一个文件中**结构和过程**描述的对比，而前者主要记录简单组合逻辑电路和简单时序电路在两种程序文件中的差异。

## 一、完成相同功能的两种描述

在 verilog 文件中，使用 assign 对一个变量进行赋值和在 always 块中对变量进行赋值在 vvp 中的体现是有差异的。一般情况下，assign 赋值语句是结构描述，而 always 块中的赋值语句是过程描述。对于完成同样的功能，有如下 verilog 语句：
```verilog
    assign  o1 =  i1 | i2;
    always@(*)begin
        o2 = i1|i2;
    end
```
在 vvp 文件中体现如下：
```vvp
...
L_00000000010ae3a0 .functor OR 1, o00000000010e3cd8, o00000000010e3d08, C4<0>, C4<0>;
...
    %load/vec4 v00000000010c33e0_0;
    %load/vec4 v00000000010ae300_0;
    %or;
    %store/vec4 v00000000010c2be0_0, 0, 1;
...
```
**由此可见**：对于结构描述，vvp 文件中使用的是 `.opcode` 的形式进行处理，而对于过程描述，则在一个线程块内使用 `%opcode` 表示。除此之外，在结构描述中的赋值是通过该功能语句一次性完成的，而在过程描述中，要经过一个“加载-执行功能-存储”的过程。

完成相同的功能包括：加减乘除运算以及各种逻辑门功能。在完成加减乘除运算时，结构描述用的是 `.arith/sum` —— “.arith” + 运算名称表示 ，逻辑门用的是 `.functor <type>` —— “.functor” + 门类型。而在过程描述中，基本运算使用 `%add` 或其他同类表示，逻辑门使用 `%and` —— “%” + 门类型表示。

## 二、阻塞赋值和非阻塞赋值

在同一个 always 块下或者说过程描述中，可以有阻塞赋值和非阻塞赋值两种赋值方式，通过符号“=”和“<=”区分。在 vvp 中，也对这两种赋值方式进行了分别处理。如有下 verilog 语句：
```verilog
    always@(*)begin
        o2 <= i1|i2;
        o1 =  i1|i2;
    end
```
显然，对于输出 o2 是非阻塞赋值，输出 o1 是阻塞赋值。在 vvp 中，相应语句如下：
```vvp
    %load/vec4 v000000000127ef20_0;
    %load/vec4 v0000000000842b40_0;
    %or;
    %assign/vec4 v0000000000842c80_0, 0;
    %load/vec4 v000000000127ef20_0;
    %load/vec4 v0000000000842b40_0;
    %or;
    %store/vec4 v0000000000842be0_0, 0, 1;
```
对比可知：

对于非阻塞赋值，vvp 文件的处理是使用 `%assign` 语句进行赋值操作。对于阻塞赋值，则是用“加载-执行功能-存储”的过程来实现。

## 三、延时
verilog 语言中有多种形式的定义延时方式，在 vvp 文件中的体现也有所不同，下面一一介绍。

**1. 结构描述的延时**
```verilog
    assign #15 o1 = i1|i2;
```
对应 vvp 语句：
```vvp
L_000000000087ef50/d .functor OR 1, o00000000010e3cd8, o00000000010e3d08, C4<0>, C4<0>;
L_000000000087ef50 .delay 1 (15,15,15) L_000000000087ef50/d;
```
即：使用 `.delay` 语句定义延时，在执行功能后，将结果输送给一个临时变量(原 label 名后加“/d”)，然后再对这个临时变量进行延时操作，延时过后再将临时变量的值赋给原变量。

**2. 过程描述的延时**
```verilog
    always@(*)begin
        #10 o2 = i1|i2;
    end
```
对应 vvp 语句：
```vvp
    %delay 10, 0;
    %load/vec4 v0000000000f3e620_0;
    %load/vec4 v0000000000f3ee50_0;
    %or;
    %store/vec4 v0000000000fc2b40_0, 0, 1;
```
可见，在过程语句中使用的是 `%delay` 函数执行延时操作。但是，对于非阻塞赋值的内部时延，会在 `%assign` 内部定义时延。
```verilog
    always@(*)begin
        o2 <=#10 i1|i2;
    end
```
对应 vvp 语句为：
```vvp
    %load/vec4 v00000000011ae570_0;
    %load/vec4 v0000000001032b40_0;
    %or;
    %assign/vec4 v0000000001032c80_0, 10;
```
在 `%assign` 的第二个 operand 中，表示的就是非阻塞赋值的内部延时。

**3. #0 延时**

在 verilog 语言中使用 `#0` 定义延时时间时，在对应的 vvp 语句中，只是将相应的时延参数改为0，其他没有任何的变动。

## 四、按位取反与逻辑取反

verilog 中有两种取反方式：按位取反(~)和逻辑取反(!)，其中按位取反是对当前变量的每一位取反，例如需要取反的值为 `0011010` ，则按位取反后，即 `!(0011010) = 1100101` ， 对于逻辑取反，则是将变量值中的每一位进行**或**运算后再取反，也就是：**当变量值的每一位都为0时，结果才为1**。

因此，在 vvp 文件中，对这两种不同的取反方式进行了不同的处理。在结构描述中：
```verilog
    assign  o1 = !i1;
    assign  o2 = ~i2;
```
```vvp
...
L_00000000008ca150 .functor NOT 1, o00000000010a3d08, C4<0>, C4<0>, C4<0>;
...
L_00000000010828b0 .reduce/nor o00000000010a3cd8;
...
```
前者是按位取反，也就是做逻辑门中的“非”门(NOT)，后者为逻辑取反，使用简短形式的或非门(`.reduce/nor`)，符合我们前面的定义。

在过程描述中：
```verilog
    always@(*)begin
        o1 = ~i1;
        o2 <= !i1+i2;
    end
```
```vvp
    %load/vec4 v00000000008b28b0_0;
    %inv;
    %store/vec4 v00000000008b29f0_0, 0, 1;
    %load/vec4 v00000000008b28b0_0;
    %nor/r;
    %load/vec4 v00000000008b2950_0;
    %add;
    %assign/vec4 v00000000008b2a90_0, 0;
```
可见，在过程描述中，vvp 使用的是 `%inv` 代表按位取反，`%nor/r` (或非的简短形式) 来代表逻辑取反。
