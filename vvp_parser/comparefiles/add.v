module add(i1,i2,o1,o2);
    input i1;
    input i2;
    output reg o1;
    output reg o2;
    wire tmp;
    //assign ;
    always@(posedge i1 or negedge i2)begin
        o1 = ~i1;
        o2 <= i1||i2;
    end
endmodule