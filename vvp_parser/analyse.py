
def is_constant(operand_str):
    if "C" in operand_str[0:3] and "<" in operand_str[0:3]:
        return True
    else:
        return False


def fill_buckets(
    verbose_flag,
    statement_list,

):
    logic_cell_dict = {}    # Will be changed
    net_dict = {}           # Will be changed
    scope_dict = {}         # Will be changed
    var_dict = {}

    port_list = []

    for statement in statement_list:            # This will not change the original list.
        label = statement.get("label")
        opcode = statement.get("opcode")

        if opcode and opcode == ".functor":
            functor, input_width = statement["operands"][0].split(' ')

            statement["type"] = functor
            statement["input_width"] = input_width      # ???
            statement["inputs"] = statement["operands"][1:]
            statement["drivers"] = [
                input for input in statement["inputs"] if not is_constant(input)
            ]

            label = statement["label"]
            logic_cell_dict[label] = statement

        elif opcode and opcode == ".resolv":
            resol_func = statement["operands"][0]

            statement["resol_func"] = resol_func
            statement["drivers"] = statement["operands"][1:]

            label = statement["label"]
            logic_cell_dict[label] = statement

        elif opcode and opcode == ".part":
            symbol = statement["operands"][0]

            statement["symbol"] = symbol
            statement["base"] = statement["operands"][1]
            statement["wid"] = statement["operands"][2]
            statement["drivers"] = []
            label = statement["label"]
            logic_cell_dict[label] = statement

        elif opcode and opcode == ".net":
            msb, lsb = statement["operands"][1].split(' ')
            statement["name"] = statement["operands"][0][1:-1]
            statement["driver"] = statement["operands"][2]
            statement["msb"] = msb
            statement["lsb"] = lsb

            label = statement["label"]
            net_dict[label] = statement

        elif opcode and opcode == ".var":
            msb, lsb = statement["operands"][1].split(' ')
            statement["name"] = statement["operands"][0][1:-1]
            statement["msb"] = msb
            statement["lsb"] = lsb

            label = statement["label"]
            var_dict[label] = statement

        elif opcode and opcode == ".scope" and label:
            statement["type"] = statement["operands"][0],   # Module
            if len(statement["operands"]) < 2:
                pass
            elif len(statement["operands"]) < 4:
                name, type_name, file, line_number = statement["operands"][1].split(
                    ' '
                )
                statement["is_top"] = True if name == type_name else False
            else:
                name, type_name, file, line_number = statement["operands"][1].split(
                    ' '
                )
                statement["parent"] = statement["operands"][3]
                statement["is_top"] = False

                statement["instance_name"] = name,
                statement["definition_name"] = type_name,

            # statement["is_dff"]:True if args.dff == tokens[3].string.split()[1] else None,  # determine if the scope is a dff.

            label = statement["label"]
            scope_dict[label] = statement

        elif opcode and opcode == ".port_info":
            number, direction, _, name = statement["operands"][0].split(' ')
            statement["name"] = name[1:-1]
            statement["direction"] = direction
            statement["number"] = number

            port_list.append(statement)
        else:
            if verbose_flag:
                print("Unknown type of statement:", label, opcode)
    return logic_cell_dict, net_dict, scope_dict, var_dict, port_list


def attach_signal_names(
    scope_dict,     # R/W
    var_dict,
    net_dict
):
    """
    Create 'signals' of a label
    """
    for var_label, var in var_dict.items():
        if scope_dict[var["scope"]].get("signals") == None:
            scope_dict[var["scope"]]["signals"] = {}
        scope_dict[var["scope"]]["signals"][var["name"]] = var_label
    for net_label, net in net_dict.items():
        if scope_dict[net["scope"]].get("signals") == None:
            scope_dict[net["scope"]]["signals"] = {}
        scope_dict[net["scope"]]["signals"][net["name"]] = net_label


def add_logic_relation(
    verbose_flag,
    logic_cell_dict,    # R/W
    var_dict,           # Will be changed
    net_dict,           # not possible?

):
    """
    Find cell driven by each cell/net/var, mark as 'drives'
    """
    for cell_label, cell in logic_cell_dict.items():
        # In a cell, the input could be cell, net, var, or constant
        for driver in cell["drivers"]:
            # The input of current cell is driven by a logic cell;
            if logic_cell_dict.get(driver):
                if logic_cell_dict[driver].get("drives"):
                    logic_cell_dict[driver]["drives"].append(cell_label)
                else:
                    logic_cell_dict[driver]["drives"] = [cell_label]
            # The input of current cell is driven by a var;
            elif var_dict.get(driver):
                if var_dict[driver].get("drives"):
                    var_dict[driver]["drives"].append(cell_label)
                else:
                    var_dict[driver]["drives"] = [cell_label]
            # The input of current cell is driven by a net; ???
            elif net_dict.get(driver):
                print("Something strange happened.")
                if net_dict[driver].get("drives"):
                    net_dict[driver]["drives"].append(cell_label)
                else:
                    net_dict[driver]["drives"] = [cell_label]
            else:
                if verbose_flag:
                    print("Unknown type of input:", driver)


def add_scope_relation(
    verbose_flag,
    port_list,      # read-only
    scope_dict      # Will be changed
):
    """
    Find children and ancestors of each scope.
    """
    for label, scope in scope_dict.items():  # the "scope" inside loop is a copy...
        scope_dict[label]["ports"] = []                     # init

        parent = scope.get("parent")
        if not parent:
            continue
        if not scope_dict[parent].get("children"):
            scope_dict[parent]["children"] = []
        scope_dict[parent]["children"].append(label)

        if not scope.get("ancestors"):
            # Can not  use "scope" here
            scope_dict[label]["ancestors"] = []
        ancestor = parent
        while scope_dict.get(ancestor):
            scope_dict[label]["ancestors"].append(
                ancestor
            )  # Can not use "scope" here
            ancestor = scope_dict[ancestor].get("parent")

    for port in port_list:
        port["signal"]=scope_dict[port["scope"]]["signals"][port["name"]]
        scope_dict[port["scope"]]["ports"].append(
            port
        )     # Attach port to scope
