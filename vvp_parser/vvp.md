# VVP文件调研

在一个vvp文件中，一般是由头定义、各行语句以及文件名组成。

本次调研已对vvp文件进行深入学习，目前可以将其分解为头定义、scope块以及文件名。

其中一个vvp文件有多个scope块，每个scope块中包含变量定义、端口信息、功能语句以及事件族等。

# 一、结构语句
## 1. Scope 语句

scope语句的语法一般为：
```
<label> .scope <type>, <name> <type-name> <file> <lineno> ;

<label> .scope <type>, <name> <type-name> <file> <lineno>, \
		       <def-file> <def-lineno> <is-cell>, <parent> ;
```

原文中的解释：(括号内为工作中得出的结果或猜想)

1. `<name>` :对于module来说，此处是实例名。对于 task 或者 function 来说，这是 task 或者 function 的名字。
2. `<type-name>` : 定义名。对于 module 和 class scope 来说，它是定义名而非实例名。
3. `<file>`,`<lineno>` ：该scope中实例的位置。

   **(经测试，`<lineno>`为实例中定义的行号)** 
4. `<def-file>`,`<def-lineno>` : 为源文件和该scope的定义行号。

   **(经测试，`<def-lineno>`为原文件中定义模块的行号)**
5. `<is-cell>` : 判定该module是否为celltype的标志，若为true（非0就是true），则是。

   **(尚不明确 celltype 所代表的含义)**
6. `<parent>` : 无相关叙述。

   **(猜想：parent为"父母"的意思，猜测其指代的是根scope的label名)**

文末指明：简短形式的 scope 语句，仅仅用于根 scope 中。

在出现一个 scope 语句时，在出现下一个 scope 语句之前所包含的内容都是前一个 scope 语句的内容。

## 2. Variable 语句

变量是一个一位向量，可以由行为代码编写(因此没有结构输入)，并将其输出传播给函数。一般语法定义为：
```
<label> .var "name", <msb> <lsb>; Unsigned logic variable
```
`"name"`即为变量名。
`<msb>、<lsb>`为该变量的最高有效位和最低有效位。

## 3. Net 语句

net 语句与 variable 语句类似，但并不完全相同。在非强制模式下，一个线程不能写入一个 net 语句。

一般语法形式为：
```
<label> .net "name", <msb>, <lsb>, <symbol>;
```
其中，`<label>` 在 net 语句中是必须的，用来定位其所代表的对象。但该label并不映射至一个 functor ，`<symbol>` 代表了该变量。

其他和 variable 语句类似。

经观察，每个 net 变量前的 `<label>` 都是仅出现一次，而其 `<symbol>` 却是变量名所指代的 label 。

## 4. Timescale 语句

此语句在官方文档Readme中并无叙述，但在工作当中，我们发现，timescale 语句一般格式为：
```
.timescale <delay_unit> <delay_precision>
```
即，在`.timescale` 后，前者为时延单位，后者为时延精度，单位为秒。**注：该数字为以十为底的幂的指数，一般为负数。**

## 5. Port_info 语句

同 timescale 语句，此语句在官方文档Readme中也并无叙述。但不难看出，该语句是描述端口信息的语句，基本格式猜测如下：
```
.port_info <num1> /<port_type> <num2> "name";
```
1. `<num1>` 为端口序号，一般按照先输出端口再输入端口的顺序排列，从0起始。
2. `<port_type>` 为端口的类型，输入端口或输出端口。
3. `<num2>` 为该端口的位数。
4. `"name"` 为端口名。

## 6. Functor 语句

functor 语句是模拟仿真的基本结构单元，包括类型以及最多4个输入。一般语法如下：
```
<label> .functor <type>, symbol_list ;
<label> .functor <type> [<drive0> <drive1>], symbol_list ;
```
其中，`symbol_list` 是其他4个functor的label，可以把他理解为，是连接在完成该功能的门上的各个输入端口。**未连接的将用C<?>代替**。问号的取值为“0 1 z x”。具体取值取决于完成功能的类型。例如：若完成的功能是“与”，而只接了2个输入，那么其他两个未连接的输入端均为“C<1>”，这样便不会影响整体取值。

**注：在 functor 中输入的强度将被忽略，而输出有固定的 `<driver0>`和`<dirver1>` 强度。因此，通过 functor 时强度信息常会被丢失。**

对于 functor 语句中的 `<label>` 经过观察猜想其为完成该功能的输出端 `<label>` 。

## 7. Thread 语句

Thread语句为仿真创建初始线程，有可能是代表 initial 块和 always 块或者其他可能开始一个线程的事件。一般语句形式如下：
```
.thread <symbol> [, <flag>]
```
该语句在 `<symbol>` 给出的指令处创建一个以此地址起始的线程。当仿真开始时，thread 语句将创建一个线程，从 `<symbol>` 地址指令开始。

对于`<flag>`，它能够影响一个线程的创建和执行。支持的 flag 是：

**$push**：将线程推入调度程序。该命令仅仅影响起始线程。因为推入线程会在非推入线程前启动。这样就可以解决起始线程冲突的问题。

## 8. Event 语句

每个线程都需要与一个网表的 functor 进行同步或异步的交互，在某些情况下，functor 需要唤醒一个等待线程，而 functor 就是通过一个 `.event` 语句启动一个线程。

evnet 语句的一般形式如下：
```
<label> .event <type>, <symbols_list>;
<label> .event "name";
```
其中，

`<type>` 描述的是能触发该事件的条件，这个条件可以是上升沿、下降沿或者边沿。如果类型是一个 "name" 字符串，那么它代表的就是通过 `%set` 指令接收到的命名事件，而不是由 functor 的输出得到。

`<symbols_list>` 是引发该事件的一系列输出。

## 9. Delay 语句
此 delay 语句和可执行指令操作符中的 "%delay" 有所不同，在延迟节点中，可以有固定延迟或可变延迟。固定延迟只有用于延时的输入，其延时值在该节点定义行中给出。可变延迟有三个额外的输入来接收用于延迟的上升、下降和衰减时间。

语句一般形式为：
```
	.delay <width> ( <rise>, <fall>, <decay> ) <input> ;
	.delay <width> <input>, <rise>, <fall>, <decay> ;
```
**注**： 实际 vvp 语句中，在 `.delay` 前存在一个 `label` ，如下所示：
```
L_0000000004545120 .delay 1 (7,7,7) L_0000000004545120/d;
```
上面的第一种形式采用三个常量(64位)作为初始延迟，并带有一个输入。第二种形式有4个 net 型输入，第一个是要延迟的值，而剩下的都是延迟所需要用到的值。
## 10. Reduction Logic 语句

“简化逻辑”/归约逻辑语句，仅接受一个向量，并且输出一个比特。

语句有以下形式：
```
        <label> .reduce/and  <symbol> ;
        <label> .reduce/or   <symbol> ;
        <label> .reduce/xor  <symbol> ;
        <label> .reduce/nand <symbol> ;
        <label> .reduce/nor  <symbol> ;
        <label> .reduce/xnor <symbol> ;
```
`.reduce` 斜杠后的为执行的逻辑功能，此语句完成将输入进来的信号进行逐位运算并将结果输出的功能。`<symbol>` 为要进行运算的变量 label 名。

## 11. Part select 语句

Part select (部分选择) 语句是带有3个输入的 functor 。该语句在端口0接收一个向量，并输出该向量的一部分(可能更少)。其他输入可以指定这些部分的位数和宽度。标准语句如下：
```
<label> .part <symbol>, <base>, <wid>;
```
输入通常是 `.reg` 或 `.net` ，但也可以是网表中的任何向量节点。

base可能是基于某位。

## 12. Part concatenation 语句

与部分选择语句相反的是部分连接语句。该语句接受输入向量值并产生一个单个向量输出，该输出是所有输入的串联。

语句结构为：
```
<label> .concat [W X Y Z], <symbols_list> ;
```
"[" 和 "]"标记框着一组4个数字，这些数字是所有输入的预期宽度。这些宽度用于表示生成的输出中输入向量的位置，并按照 LSB 到 MSB 的顺序列出。初始的输出为 (W+X+Y+Z)'bx。当输入值被传播出去时，输出的位数将放在输出向量值的正确位置中，并且同时一个新的输出值被传播出去。

## 13. Resolver 语句
Resolver 语句是具有4个输入的可感知强度的 functor ，但它们的作用通常是使用强度解析来计算输出。
```
<label> .resolv tri,  <symbols_list>;
<label> .resolv tri0, <symbols_list>;
<label> .resolv tri1, <symbols_list>;
```
来自 resolver 的输出是一个 vvp_vector8_t 的值，也就是说，该输出是包含强度信息的向量。

## 14. Parameter 语句

Parameters(参数) 是在一个 scope 内的命名常量。这些参数有类型和对应的值，还有一个 label ，以便它们可以作为 VPI 对象被引用。一个 Parameter 语句的语法一般为：
```
<label> .param/str <name>, <value>;
<label> .param/b <name>, <value> [<msb>,<lsb>];
<label> .param/l <name>, <value> [<msb>,<lsb>];
<label> .param/r <name>, <value>;
```
其中：

1. `<name>` 是代表该参数名字的字符串。名称作为 vpiParameter 对象放置在当前 scope 中。
2.  .param/str -- 该参数有一个字符串值。
3.  .param/b   -- 该参数有一个布尔向量值。
4.  .param/l   -- 该参数有一个逻辑向量值。
5.  .param/r   -- 该参数有一个实值。

因此，参数的值适合于数据类型。例如：
```
P_123 .param/str "hello", "Hello, World.";
```
此外，布尔值和逻辑值也可以有符号或无符号。如果有符号，则在值前面加上一个“+” 字符。(注意，该值是2进制的补码，所以 “+” 只表示它是有符号的，而不是正数。)

## 15. Array index 语句

Array idex(数组索引)语句用于收集变量。


## 16. UDP 语句

UDP语句要么定义用户定义的原语，要么通过创建 UDP functor 实例化之前定义的 UDP。UDP functor 具有与 UDP 定义要求相同的输入。UDP 有**连续**和**组合**两种形式。

连续的 UDP 带有一个输出状态，并且可以对输入边沿作出响应。但是组合 UDP 的输出仅仅取决于当前输入，或者说，其输出是当前输入的函数。

UDP 的功能是通过真值表定义的。表的每一行是描述输入状态或边的字符串，以及新的输出状态。组合 udp 要求每个输入使用一个字符，在输出状态结束时使用一个字符。连续 udp需要为当前状态添加一个字符，即该行的第一个字符。任何输入转换或新状态最多只能匹配一行(或所有匹配必须提供相同的输出状态)，如果没有匹配的行，则输出变为1'bx。

输出状态可以为 “0”、“1”或 “x”。连续 udp 也可以有 “-”，代表没有变化。

## 17. Structural shifter 语句

结构上下文中的变量移位是用.shift语句实现的：
```
<label> .shift/l <wid>, <data symbol>, <shift symbol>;
<label> .shift/r <wid>, <data symbol>, <shift symbol>;
```
`wid` 为移位器定义输出向量宽度，`<data symbol>` 是要移动的输入数据，`<shift symbol>` 是要移动的数量。来自端口0的向量是要移位的数据，并且必须与输出的宽度相同。端口1的输入是要移位的数量。
`l/r` 分别代表左移和右移。

# 二、过程语句指令

指令操作码都以%字符开头，并且有0个或多个操作数，但任何情况下操作数都不超过三个。
## 1. %pushi/vec4

此操作码将一个立即值 vector4 加载到向量堆栈中。语句的一般形式为：
```
%pushi/vec4 <vala>, <valb>, <wid>
```
其中，`<vala>` 是一个布尔型值，`<valb>` 是用于支持z和x值的修饰符。a 与 b 有四种编码组成方式，分别对应4种逻辑值，列表如下：

|   a   |   b   |  val  |
| :---: | :---: | :---: |
|   0   |   0   |   0   |
|   1   |   0   |   1   |
|   1   |   1   |   x   |
|   0   |   1   |   z   |

也即：当 b=0 时，赋值的内容就是 a 的值。

`<wid>` 为值的位宽。

## 2. %store/vec4

此操作码将一个逻辑向量存进变量中。语句的一般形式为：
```
 %store/vec4 <var-label>, <offset>, <wid>
```
值(及其宽度)从堆栈顶部弹出并写入变量。

`<offset>` 是索引寄存器，它包含写入一部分变量的的部分偏移量。如果该值为0，就代表字面上的0，而不用再从索引寄存器中取值。

## 3. %delay

此操作码能够暂停线程，并在未来重新调度一段时间执行。语句一般形式如下：
```
%delay <low>, <high>
```

```verilog
# 100
```
vvp :
```
%delay 100, 0;
```
## 4. %load

该指令从给定的 functor 节点加载一个 vector 值，并将其压入vec4堆栈。

语句一般形式为：
```
 %load/vec4 <var-label>
```
其作用基本和 `%store/vec4` 类似。

## 5. %jmp

`%jmp` 指令执行时，程序将跳转到制定位置。其参数是目标指令的 label 。

特殊地，有以下形式：

`%jmp/[0xz] <code_label>, <flag> `

这是 `%jmp` 的特殊形式，若 `<flag>` 位的值是 “/” 后的其中一个，则程序的线程将跳转至 `<code_label>` 所在的线程。

**注**：经测试，对官方文档中所提及的 `<flag>` 位用法不是很理解。

## 6. %and

此指令对从 vec4 堆栈中弹出的两个 vector 进行按位“与”，并且将其结果推入。and 中每一位都是独立于其他位计算的。输入 vector 必须是相同的宽度，输出 vector 将是输入 vector 的宽度。

## 7. %flag_set/vec4
```
%flag_set/vec4 <flag>
```
该指令提供了设置标记位的方法。主要作用是将 vec4 堆栈的顶部弹出并将最低有效位写入选定的标志。

## 8. %inv

在vec4堆栈的顶部对变量执行逐位反转，即取反功能。

## 9. %cmpi/e

该指令的一般形式为：
```
%cmpi/e <vala>, <valb>, <wid>
```
`%cmp` 指令族对两个大小相同的向量进行一般比较。它们从栈顶提取两个值进行比较，而不是替换值。比较的结果将被写入标志位4、5、6中。计算表达式 (a<b)、(a==b)和(a===b)时，首先从堆栈中取出(b)，然后取出(a)，再进行比较。

标志位定义：

4：eq (equal)
5: lt (less than)
6: eeq (case equal)

其中，当向量中的所有位都完全相等时， `eeq` 标志位将被置1，否则置0。 当两个向量比较结果为逻辑相等时，`eq` 标志位将被置1。**注**：“x” 和 “z” 被认为是逻辑相等的。换句话说，`eq` 标志位就相当于 “==” 符号，而 `eeq` 标志位相当于 “===” 符号。

`lt` 标志位：当左边向量小于右边向量时，将其置1。

对于 `%cmpi/e` 语句，它不用计算 `lt` 标志位，也即：当 `lt` 标志位不必计算时，该语句执行速度更快。

## 10. %assign/vec4
```
%assign/vec4 <var-label>, <delay>
```
此指令执行非阻塞赋值。将堆栈中传来的值分配给`<var-label>` 中。 `<delay>`是时延系数，代表应该在哪个时刻进行赋值。要进行赋值的值从 vec4 堆栈中弹出。非阻塞赋值有内部延时，若在赋值前进行延时的话，`<delay>` 将保存该时延值。

# 二、vpi 任务/函数调用语句

线程中要调用 vpi 任务时，通常使用 `%vpi_call` 或者 `vpi_func` 指令。语句一般如下：
```
%vpi_call <file-index> <lineno> <name>, <args>... ;
```
其中，

`<file-index>` 为对字符串表的索引号。

`<lineno>` 为该任务或函数在源代码中出现的行号。

`<name>` 是一个字符串，指代该系统任务或函数的名字。

`<args>` 是由逗号分隔的一系列参数。


# 三、事件

这里暂不讨论事件语句，仅仅讨论事件。

**vvp文件中，只有3种事件类型：Propagation events、Assingnment events 以及 Thread schedule events**，下面分别介绍。

## 1. Propagation events

Propagation 事件仅仅指向输出被传播的 functor ，并且在特定时间执行。当此事件结束时，引用 functor 的输出将被传播到所有连接在该 functor 的输入中，当有必要时，这些 functor 会创建新的事件。

## 2. Assingnment events

Assingnment 事件由行为代码中的非阻塞赋值创建。当执行非阻塞符号“<=”时，将创建一个分配事件，其中包括了一个 vvp_ipoin_t 指针指向 functor 的输入而获取它的值。Assingnment 事件与 Propagation 事件并不相同，主要表现为以下两个方面：在 Assignment 事件中

+ a) 没有 functor 输出要赋值的值

## 3. Thread schedule events

该事件仅仅指向一个要执行的线程。线程是由一个带有程序计数器的虚拟处理器和一些私有存储组成的。Thread schedule 事件可以执行 `%assign` 指令来创建一个 `assignment` 事件；也可以执行 `%set` 指令来执行阻塞赋值；还能使用 `%load` 读取 functor 中的输出。

## 总结

核心事件调度器接受这三种事件，并调用代码使得在设计中抛出正确的事件。如果当前事件是 propagate 或是 assignment 事件，functor 的网络将被激活；如果是 thread schedule 事件，则将会启动一个线程。事件队列的执行并不重要，因为它是作为一个“跳跃表”执行的。也就是说，他是一个排好序的单链表，但是带有能够跳过 “delta-time” 事件的跳过指针。

一个线程和一个 functor 的通信通过 `%set`, `%assign`, `%waitfor` 以及 `%load` 指令实现。对于一个线程来说， functor 网络是一个通过一定的访问指令进行通信的结构群。

# 四、vvp的编译和执行过程

vvp程序的执行过程一般分为以下4个部分：

+ **1. 初始化 (Initialization)**：清空数据结构以及链表，为编译做准备。
+ **2. 编译(Compilation)**：读取并编译输入文件，并根据需要建立符号列表以及分配对象。
+ **3. 清理(Cleanup)**：释放符号表和其他仅用于编译的资源，以减少内存占用。
+ **4. 仿真(Simulation)**：开始运行仿真事件。
  

具体地说：

**初始化**进程由 `compile.cc` 中的 `compile_init()` 函数执行。该函数按顺序调用源码中其他需要为编译进行初始化的部分中的所有 `*_init()` 函数。所有子初始化函数称为 `<foo>_init()` 。

**编译**过程由 parser 控制，当 parser 读取和 parse 输入时，编译过程通过调用各种 `compile_*` 函数执行。所有这些函数都在 `compile.cc` 中，而且编译过程会根据需要调用代码的其他部分。

在 parser 完成编译时，将调用 `compile_cleanup()` 函数来完成接下来的编译过程。完成各级调用后，所有的符号表和其他编译需要的资源都将被释放。一旦 `compile_cleanup()` 函数有了返回值，对 `compile.cc` 中函数的 parser 都将停止工作。

在**清理**过程结束后，**仿真**过程便开始了。仿真过程是通过执行 `schedule_simulate()` 函数完成的，他会完成最后的配置，并开始运行仿真过程以及处理事件队列。

# 五、verilog到vvp的部分语句及功能的转换

**门级描述**

如前所述，在vvp文件中，完成verilog中基本逻辑门的功能是由 functor 语句实现的。但是 functor 语句不能超过4个输入，如果超过4个输入， functor 语句将自动将其级联。

如：

verilog 语句中描述一个“与”门：
``` verilog file
and gate (out, i1, i2, i3);
```
转化为 vvp 语句为：
```
out	.functor and, i1, i2, i3;
```
前面已经介绍过， functor 语句后的第一个参数是其类型。

当超过4个输入时：
``` verilog file
and gate (out, i1, i2, i3, i4, i5, i6, i7, i8);
```
转化为 vvp 语句为：
```
	gate.0	.functor and, i1, i2, i3, i4;
	gate.1	.functor and, i5, i6, i7, i8;
	gate	.functor and, gate.0, gate.1;
```

**变量定义**

reg 型以及整型变量，在 vvp 文件中将转化为 variable 语句。其他的例如 wire 型的变量将转化为 net 语句。
如：

``` verilog
reg a;
```
转化为 vvp 语句为：
```
a    .var "a",0 0;
```




# 六、Verilog 的建模

HDL语言用于建模时，可以根据抽象的层次进行分类：

可以用高级别的的语法描述需求和概念（**系统级**）

可以将需求概念用数学形式描述（**算法级**）

可以用通用寄存器的传输操作来描述（**RTL寄存器传输级**）

可以将通用寄存器描述的模型转变为逻辑门描述的模型（**Gate-level门级**）

可以将逻辑门描述的模型转变为用电路开关描述的模型（**Switch-level开关级**）

HDL建模时，除了可以用不同层次的抽象进行分类，还可以根据其对信号的描述方式的不同划分为以下三种：

1. 数据流建模
2. 行为建模
3. 结构化建模

其中描述电路行为的建模就是**行为建模**，其代码就是**描述行为的代码(Behavioral code)**