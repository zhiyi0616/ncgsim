import json
import vvp

############
#FUNCTIONS
############

def is_constant(operand_str):
    if "C" in operand_str[0:3] and "<" in operand_str[0:3]:
        return True
    else:
        return False

def statement_classify(statement_tokens=[]):          #every token will be classfied as logic_cell、var or net.
    if len(statement_tokens)>3:
        if statement_tokens[1].type=="OPCODE" and statement_tokens[1].string in".functor"+".resolve"+".part"+".delay":
            return "logic_cell",statement_tokens
        elif statement_tokens[1].type=="OPCODE" and statement_tokens[1].string==".var":
            return "var",statement_tokens
        elif statement_tokens[1].type=="OPCODE" and statement_tokens[1].string==".net":
            return "net",statement_tokens
        elif statement_tokens[1].type=="OPCODE" and statement_tokens[1].string==".scope":
            return "scope",statement_tokens
        else:
            return "other",statement_tokens
    elif len(statement_tokens)==2 and statement_tokens[0].string==".thread":
        return "thread",statement_tokens
    else:
        return "other",statement_tokens


class FunctionalStatement:  # procedural, structural and label_only
    def __init__(self,tokens:list = []):
        assert tokens
        self.label = ""
        self.opcode_str = ""
        self.operand_strs = []
        self.comment_str = ""
        self.type = ""

        token_cnt = 0

        # Check empty
        if not len(tokens):
            self.type="empty"
            return 
        
        # Delete comment
        if tokens[-1] and tokens[-1].type == "CMT":
            tokens = tokens[:-1]
        
        # Deal with label
        if tokens[token_cnt].type=="LABEL":
            self.label = tokens[token_cnt].string
            token_cnt += 1
        
        # Return if label-only
        if token_cnt>=len(tokens):
            self.type = "label_only"
            return 

        # Deal with opcode
        if tokens[token_cnt].type=="OPCODE" :
            self.opcode_str = tokens[token_cnt].string
            if tokens[token_cnt].string==".functor":
                self.type = "functor"
            else:
                pass
            token_cnt += 1
        else:
            raise Exception("No opcode found and not label-only.")
            
        # Operands
        if token_cnt <len(tokens):
            operands = tokens[token_cnt  :] if tokens[-1].type!="CMT" else tokens[token_cnt  : -1]
            self.operand_strs = [oa.string for oa in operands]
        

##################
## LOGIC_CELL
##################
class LogicCell:
 def __init__(
        self,
        tokens,
        current_scope:str
    ): 
        assert tokens 
        assert current_scope
        token_cnt = 0
        operand_cnt = 0
        operands = []
        self.label = ""
        self.type = ""
        self.inputs=[]
        self.drivers=[]
        self.scope = ""
        # self.drives = []

        # Deal with label
        if tokens[token_cnt].type=="LABEL":
            self.label = tokens[token_cnt].string
            token_cnt += 1
        else:
            raise Exception("The cell do not have a label.")

        # Deal with opcode
        if tokens[token_cnt].type=="OPCODE" :
            if tokens[token_cnt].string==".functor":
                self.type = "functor"
            elif tokens[token_cnt].string==".resolv":
                self.type = "resolv"
            elif tokens[token_cnt].string==".concat":
                self.type = "concat"
            elif tokens[token_cnt].string==".part":
                self.type = "part"
            elif tokens[token_cnt].string==".delay":
                self.type = "delay"
            operands = tokens[token_cnt +1 :] if tokens[-1].type!="CMT" else tokens[token_cnt +1 : -1]
        
        # Deal with operands
        for operand in operands:
            if self.type == "functor":
                if operand_cnt == 0:
                    split_blank=operand.string.split(" ")   # OPERAND will be divided into functor and inputs_width
                    self.functor=split_blank[0]
                    self.input_width=split_blank[1]
                else:
                    self.inputs.append(operand.string)
                    if not is_constant(operand.string):
                        self.drivers.append(operand.string)
            elif self.type == "resolv":
                if operand_cnt == 0:
                    self.resol_func = operand.string         # The first operand of a resolv statement is the resolution function
                else:
                    self.inputs.append(operand.string)       # Others are inputs

            elif self.type == "concat":
                pass

            elif self.type == "part":
                if operand_cnt == 0:
                    self.symbol = operand.string             # The first operand of a part statement is a symbol
                elif operand_cnt == 1:
                    self.base = operand.string               # The second is the base
                else:
                    self.input_width = operand.string        # The last is the input width

            elif self.type == "delay":
                split_blank = operand.string.split(" ")
                # FIXME: deal with delay
            operand_cnt += 1
        self.scope=current_scope
          
##############
##VAR
##############

class Var:
    def __init__(
        self,
        tokens,
        current_scope): 
        
        token_cnt = 0
        operand_cnt = 0
        assert tokens and current_scope

        # Deal with label
        if tokens[token_cnt].type=="LABEL":
            self.label = tokens[token_cnt].string
            token_cnt += 1
        else:
            raise Exception("The cell do not have a label.")

        # Deal with operands

        if tokens[token_cnt].type == "OPCODE":
            token_cnt += 1
            operands = tokens[token_cnt :] if tokens[-1].type!="CMT" else tokens[token_cnt : -1]

        for operand in operands:
            if operand_cnt == 0:
                self.name = operand.string
            else:
                split_blank=operand.string.split(" ")   # The second OPERAND will be divided into MSB and LSB
                self.MSB = split_blank[0]               # MSB means the Most Significant Bit
                self.LSB = split_blank[1]               # LSB means the Lowest Significant Bit
            operand_cnt += 1

            self.scope=current_scope

##############
##NET
##############
class Net:
    def __init__(
        self,
        tokens,
        current_scope): 

        token_cnt = 0
        operand_cnt = 0
        assert tokens and current_scope

        # Deal with label
        if tokens[token_cnt].type=="LABEL":
            self.label = tokens[token_cnt].string
            token_cnt += 1
        else:
            raise Exception("The cell do not have a label.")

        # Deal with operands
        if tokens[token_cnt].type == "OPCODE":
            token_cnt += 1
            operands = tokens[token_cnt :] if tokens[-1].type!="CMT" else tokens[token_cnt : -1]

        for operand in operands:
            if operand_cnt == 0:
                self.name = operand.string
            elif operand_cnt == 1:
                split_blank=operand.string.split(" ")   # The second OPERAND will be divided into MSB and LSB
                self.MSB = split_blank[0]               # MSB means the Most Significant Bit
                self.LSB = split_blank[1]               # LSB means the Lowest Significant Bit
            elif operand_cnt == 2:
                self.driver = operand.string
            operand_cnt += 1

        self.scope=current_scope

###############
##THREAD
###############

class Thread:
    def __init__(
        self,
        label,                                          # The operand behind a ".thread"
        thread_proc_list
    ):
        self.statements = []
        find_thread = False                                 # Show that if a thread is found
        for thread_row in thread_proc_list:
            
            self.label = label
            if label == thread_row[0].string and thread_row[0].type=="LABEL":  #  Find the thread start label
                find_thread=True
            if find_thread == True:
                self.statements.append([token.__dict__ for token in thread_row])    # Store the parsed thread tokens


