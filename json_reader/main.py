import networkx as nx
import matplotlib.pyplot as plt
import json

if __name__ == '__main__':
    import argparse
    json_data = None

    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("input_file")
    args=arg_parser.parse_args()
    with open(args.input_file) as f:
        json_data = json.load(f)

    G = nx.DiGraph()
    for cell_label, cell in json_data["logic_cells"].items():
        print(cell)
        G.add_node(cell_label)
        for driver in cell["drivers"]:
            G.add_edge(driver, cell_label)

    nx.draw(G, with_labels=True)
    plt.show()
    