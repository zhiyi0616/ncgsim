# NCGSim
A gate-level simulator, managed by NCU.

| markdown文档名                         | 链接                                                                                                             |
| -------------------------------------- | ---------------------------------------------------------------------------------------------------------------- |
| Code structure docs of iVerilog        | [Code structure docs of iVerilog.md](code-structure-docs-of-iverilog\Code%20structure%20docs%20of%20iVerilog.md) |
| README                                 | [README.md](dc-lab-practices/lab2013-1/README.md)                                                                |
| README                                 | [README.md](debug_files/adder_and_multiplier/README.md)                                                          |
| README                                 | [README.md](debug_files/counter_16/README.md)                                                                    |
| 1. 单异或门路径延迟仿真                | [单异或门路径延迟仿真.md](debug_files/xor_single/1.%20单异或门路径延迟仿真.md)                                   |
| 2. 单异或门模块和路径延迟仿真          | [单异或门模块和路径延迟仿真.md](debug_files/xor_single/2.%20单异或门模块和路径延迟仿真.md)                       |
| 3. 单异或门不同边沿路径延迟仿真        | [单异或门不同边沿路径延迟仿真.md](debug_files/xor_single/3.%20单异或门不同边沿路径延迟仿真.md)                   |
| iVerilog 概览                          | [iVerilog 概览.md](DOCs/iVerilog%20概览.md)                                                                      |
| table_drawing                          | [table_drawing.md](DOCs/table_drawing.md)                                                                        |
| Verilog 事件-仿真概念                  | [Verilog 事件-仿真概念.md](DOCs/Verilog%20事件-仿真概念.md)                                                      |
| 事件抛出和调度                | [事件抛出和调度](DOCs/iVerilog%20VVP%20事件的抛出和执行.md)                                                      |
| 门级延迟的定义                         | [门级延迟的定义.md](DOCs/门级延迟的定义.md)                                                                      |
| 数字设计前端速查                       | [数字设计前端速查.md](DOCs/数字设计前端速查.md)                                                                  |
| README                                 | [README.md](iverilog-core/README.md)                                                                             |
| README                                 | [README.md](py-cpu-fixed_step/README.md)                                                                         |
| 关于数字电路                           | [关于数字电路.md](py-cpu-fixed_step/关于数字电路.md)                                                             |
| README                                 | [README.md](py-gpu-test/README.md)                                                                               |
| 环境安装                               | [环境安装.md](py-gpu-test/环境安装.md)                                                                           |
| README                                 | [README.md](py-vvp-parser/README.md)                                                                             |
| vvp_opcodes_copy                       | [vvp_opcodes_copy.md](py-vvp-parser/vvp_opcodes_copy.md)                                                         |
| vvp_README_copy                        | [vvp_README_copy.md](py-vvp-parser/vvp_README_copy.md)                                                           |
| [BACKUP] Installing Synopsys softwares | [[BACKUP] Installing Synopsys softwares.md](supportive_works/[BACKUP]%20Installing%20Synopsys%20softwares.md)    |
| [BACKUP] 部署 Gitlab                   | [[BACKUP] 部署 Gitlab.md](supportive_works/[BACKUP]%20部署%20Gitlab.md)                                          |
| VCS 调试                               | [VCS 调试.md](supportive_works/VCS%20调试.md)                                                                    |
| Readme                                 | [Readme.md](vvpLexer/Readme.md)                                                                                  |
